# Limite de 7 faltas (dias), mais do que 7 faltas está reprovado

| #  | Nome                                    | Faltas (dias) | 01/03 | 08/03 | 13/03 | 15/03 |
|----|-----------------------------------------|:-------------:|-------|-------|-------|-------|
| 1  | Aharon Campoli Tono                     |       4       | 0     | 0     | 0     | 0     |
| 2  | Bárbara Guimarães Klotz De Azeredo      |       1       | 2     | 0     | 2     | 2     |
| 3  | Bruna Carolina Silva Ferreira           |       2       | 2     | 0     | 2     | 0     |
| 4  | Bruno Pitman Berniz Gomes               |       0       | 2     | 2     | 2     | 2     |
| 5  | Dennis Gonçalves Lemes                  |       0       | 2     | 2     | 2     | 2     |
| 6  | Fernanda Cristina Peixoto Coelho        |       0       | 2     | 2     | 2     | 2     |
| 7  | Fernando Alves Santos                   |       2       | 2     | 0     | 2     | 0     |
| 8  | Jenifer Aline Domingues Oliveira        |       0       | 2     | 2     | 2     | 2     |
| 9  | João Pedro Kincheski Fassina            |       0       | 2     | 2     | 2     | 2     |
| 10 | Larissa Moreira Sydorak                 |       0       | 2     | 2     | 2     | 2     |
| 11 | Leonardo Simioni Da Silva               |       3       | 0     | 0     | 2     | 0     |
| 12 | Lucas Akio Paiva Miyasaki               |       0       | 2     | 2     | 2     | 2     |
| 13 | Maxim Dmitri Lobkov                     |       0       | 2     | 2     | 2     | 2     |
| 14 | Melissa Marques De Araujo               |       2       | 2     | 0     | 0     | 2     |
| 15 | Murilo Stellfeld De Oliveira Poloi      |       0       | 2     | 2     | 2     | 2     |
| 16 | Stephanie Caroline De Souza Pereira     |       1       | 2     | 0     | 2     | 2     |
| 17 | Victor Gabriel Porfírio Carazzai Zerger |       0       | 2     | 2     | 2     | 2     |
| 18 | Vitor Couto De Avanço                   |       0       | 2     | 2     | 2     | 2     |
| 19 | Wilson Drapeynski Junior                |       3       | 2     | 0     | 0     | 0     |
| 20 | Lucas Placido dos Santos                |       0       | 2     | 2     | 2     | 2     |
| 21 | Henrique Luiz Rieger                    |       2       | 2     | 2     | 0     | 0     |
| 22 | Lucas Ferreira Nogueira                 |       2       | 2     | 2     | 0     | 0     |
| 23 | Vianney Arthur Gokamoult                |       3       | 2     | 0     | 0     | 0     |
| 24 | Vinicius Matheus Comarello Ferreira     |       2       | 2     | 2     | 0     | 0     |
| 25 | Gabriel Razzolini Pires de Paula        |       1       |       |       | 2     | 0     |

























