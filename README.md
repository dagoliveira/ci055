# Algoritmos e Estruturas de Dados I

## Professor

- [Daniel Alfonso Gonçalves de Oliveira](http://www.inf.ufpr.br/dagoliveira)
- Departamento de informática - sala 60

## Acompanhamento 2019/1
- [Acompanhamento notas e frequência](./acompanhamento/acompanhamento-2019-1.md)
- [Prova 2](./acompanhamento/prova2-2019-1.md)
- [Prova 3](./acompanhamento/prova3-2019-1.md)

## Informações gerais
- [Página principal da disciplina](http://www.inf.ufpr.br/cursos/ci055/)
- [Plano de aulas](http://www.inf.ufpr.br/cursos/ci055/aulas.html)
- [Aulas em vídeo](http://www.inf.ufpr.br/cursos/ci055/20151/aulas.html)
- [Provas Antigas](http://www.inf.ufpr.br/cursos/ci055/provas-antigas.html)
- Turma C: Quartas e Sextas 15:30 - 17:10 sala PA-04

## Material
- [Slides](./slides)
- [Códigos](./codigos)
- [Listas de Exercícios](./listas)

## IDE on-line (compilador on-line)

Os seguintes links podem ser usados para compilar e executar programas pascal sem a necessidade de instalar nada na máquina local:
- [http://www.compileonline.com/compile_pascal_online.php](http://www.compileonline.com/compile_pascal_online.php)
- [https://rextester.com/l/pascal_online_compiler](https://rextester.com/l/pascal_online_compiler)
- [https://www.jdoodle.com/execute-pascal-online](https://www.jdoodle.com/execute-pascal-online)

## Frequência mínima

Por ser um curso de 60 horas/aula, o aluno que faltar mais de 25% (15 horas/aula) será reprovado, ou seja, faltar 8 dias letivos ou mais estará reprovado

## Avaliação

A avaliação será composta por três provas: P1, P2 e P3.

Média = (P1 + 2*P2 + 3*P3)/6.

Não haverá provas substitutivas, a menos nos casos previstos na resolução 37/97.

Será **aprovado** o aluno que tiver frequência mínima de 75% e média igual ou superior a 70: (frequencia >= 0.75) **and** (média >= 70)

Será **reprovado** quem não tiver frequência mínima de 75% ou média menor do que 40: (frequencia < 0.75) **or** (média < 40)

### Prova Final

Alunos com frequência mínima e média entre 40 e 69 poderão fazer uma prova final:  (frequencia >= 0.75) **and** (média >= 40) **and** (média < 70).

Média final = (Média + Prova Final) / 2 

Será **aprovado** o aluno que fizer a prova final e obtiver uma média final maior ou igual a 50: (Média final >= 50)

