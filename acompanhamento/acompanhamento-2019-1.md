| #      | GRR         | Faltas | Prova 1 | Prova 2 | Prova 3 | Média | Situação        | Exame | Média final | Situação final  |
|--------|-------------|--------|---------|---------|---------|-------|-----------------|-------|-------------|-----------------|
| 1      | GRR20185587 | 21     |         |         |         |       | Reprovado Freq. |       |             | Reprovado Freq. |
| 2      | GRR20190611 | 1      | 80      | 31      | 55      | 51,17 | Exame           | 80    | 65,58       | Aprovado        |
| 3      | GRR20185688 | 8      | 92      | 19      |         | 21,67 | Reprovado Freq. |       |             | Reprovado Freq. |
| 4      | GRR20185691 | 5      | 10      | 5       |         | 3,33  | Reprovado       |       | 3,33        | Reprovado       |
| 5      | GRR20185693 | 0      | 93      | 44      | 68      | 64,17 | Exame           | 65    | 64,58       | Aprovado        |
| 6      | GRR20185695 | 0      | 95      | 58      | 15      | 42,67 | Exame           | 72    | 57,33       | Aprovado        |
| 7      | GRR20185690 | 6      | 85      | 32      | 40      | 44,83 | Exame           | 40    | 42,42       | Reprovado       |
| 8      | GRR20185702 | 0      | 60      | 13      | 5       | 16,83 | Reprovado       |       | 16,83       | Reprovado       |
| 9      | GRR20185694 | 0      | 95      | 98      | 100     | 98,50 | Aprovado        |       | 98,50       | Aprovado        |
| 10     | GRR20185696 | 2      | 90      | 52      | 80      | 72,33 | Aprovado        |       | 72,33       | Aprovado        |
| 11     | GRR20185678 | 5      | 80      | 19      | 15      | 27,17 | Reprovado       |       | 27,17       | Reprovado       |
| 12     | GRR20185709 | 0      | 99      | 90      | 100     | 96,50 | Aprovado        |       | 96,50       | Aprovado        |
| 13     | GRR20190396 | 0      | 75      | 62      | 55      | 60,67 | Exame           | 40    | 50,33       | Aprovado        |
| 14     | GRR20185692 | 0      | 48      | 8       | 0       | 10,67 | Reprovado       |       | 10,67       | Reprovado       |
| 15     | GRR20185701 | 3      | 90      | 14      | 15      | 27,17 | Reprovado       |       | 27,17       | Reprovado       |
| 16     | GRR20185705 | 0      | 75      | 23      | 5       | 22,67 | Reprovado       |       | 22,67       | Reprovado       |
| 17     | GRR20185707 | 1      | 98      | 70      | 90      | 84,67 | Aprovado        |       | 84,67       | Aprovado        |
| 18     | GRR20185699 | 6      | 88      |         |         | 14,67 | Reprovado       |       | 14,67       | Reprovado       |
| 19     | GRR20185697 | 2      | 100     | 95      | 75      | 85,83 | Aprovado        |       | 85,83       | Aprovado        |
| 20     | GRR20170480 | 20     |         |         |         |       | Reprovado Freq. |       |             | Reprovado Freq. |
| Médias |             |        | 80,72   | 43,12   | 47,87   | 46,97 |                 | 59,40 | 49,45       |                 |
