Program primos_2_500;
Var 
    a,b: Longint;
    primo: Boolean;
Begin
    a := 2;
    while a <= 500 do
    begin
        primo := true;
        b := 2;
        while b <= sqrt(a) do
        begin
            if (a mod b) = 0 then
                primo := false;
            b := b + 1;
        end;

        if primo then
            writeln(a);
        a := a + 1;
    end;
End.
