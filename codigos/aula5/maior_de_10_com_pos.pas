Program maior_de_10_com_pos;
Var a,maior,cont,posicao: Longint;
Begin
    read(a);
    maior := a;
    cont := 1;
    posicao := 1;
    while cont < 10 do 
    begin
        read(a);
        if a > maior then
        begin
            maior := a;
            posicao := cont + 1;
        end;
        cont := cont + 1;
    end;
    writeln('maior: ',maior,' pos: ',posicao);
End.
