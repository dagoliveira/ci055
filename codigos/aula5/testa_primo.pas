Program testa_primo;
Var 
    a,b: Longint;
    primo: Boolean;
Begin
    read(a);
    if a <= 1 then (* Se o numero for menor ou igual a 1, nao faz nada *)
        writeln('Entre com um numero maior do que 1')
    else (*Se o numero for maior do que 1, testa se eh primo *)
    begin
        primo := true;
        b := 2;
        while b <= sqrt(a) do
        begin
            if (a mod b) = 0 then
                primo := false;
            b := b + 1;
        end;

        if primo then
            writeln('Eh primo')
        else
            writeln('Nao eh primo');
    end;
End.
