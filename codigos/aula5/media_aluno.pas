Program media_aluno;
Var media: Real;
Begin
    read(media);
    if media >= 9.0 then
        writeln('Aprovado, muito bem')
    else if media >= 8.0 then
        writeln('Aprovado')
    else if media >= 7.0 then
        writeln('Aprovado, por pouco')
    else
        writeln('Reprovado');
End.
