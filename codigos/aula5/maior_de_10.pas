Program maior_de_10;
Var a,maior,cont: Longint;
Begin
    read(a);
    maior := a;
    cont := 1;
    while cont < 10 do 
    begin
        read(a);
        if a > maior then
            maior := a;
        cont := cont + 1;
    end;
    writeln('maior: ',maior);
End.
