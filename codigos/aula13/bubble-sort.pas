program Bubble;
const MAX=10;
type vetor=array [1..MAX] of longint;
var
   a: vetor;
   i: integer;

(*Troca dois valores usando passagem por referencia*)
procedure troca(var a,b: longint);
var temp: longint;
Begin
    temp := a;
    a := b;
    b := temp;
End;

(*Mostra todos os elementos na tela, numa linha somente*)
procedure imprime(var v: vetor; tam: longint);
var i: longint;
begin
    for i:=1 to tam do
        write(v[i],' ');
    writeln('');
end;
 
(*Ordena um vetor usando o metodo bolha*)
procedure bubble_sort(var v: vetor; tam: longint);
var
   i, j: longint;
begin
     for i := 1 to (tam-1) do
     begin
          for j := 1 to (tam-1) do
          begin
               if v[j] > v[j+1] then
               begin
                    troca(v[j], v[j+1]);
               end;
          end;
     end;
end;
 
(*Programa principal*)
begin
    randomize; (*Uma vez somente no programa*)
    for i := 1 to MAX do
    begin
         a[i] := Random(99); (*Gera numero aleatorio entre 0 e 99*)
    end;
    writeln('Vetor inicial: ');
    imprime(a, MAX);
 
    bubble_sort(a, MAX);
 
    writeln('Vetor ordenado: ');
    imprime(a, MAX);
end.
