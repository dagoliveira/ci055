program Bubble;
const MAX=10;
type vetor=array [1..MAX] of longint;
var
   a: vetor;
   i: integer;

(*Mostra todos os elementos na tela, numa linha somente*)
procedure imprime(var v: vetor; tam: longint);
var i: longint;
begin
    for i:=1 to tam do
        write(v[i],' ');
    writeln('');
end;
 
(*Ordena um vetor usando o metodo de insercao*)
procedure insertion_sort(var v: vetor; tam: longint);
var
   i, j, temp: longint;
begin

    for i := 2 to tam do
    begin
        temp := v[i];
        j := i;
        while ((j > 1) and (v[j-1] > temp)) do
        begin
            v[j] := v[j-1];
            j := j - 1;
        end;
        v[j] := temp;
    end;
end;
 
(*Programa principal*)
begin
    randomize; (*Uma vez somente no programa*)
    for i := 1 to MAX do
    begin
         a[i] := Random(99); (*Gera numero aleatorio entre 0 e 99*)
    end;
    writeln('Vetor inicial: ');
    imprime(a, MAX);
 
    insertion_sort(a, MAX);
 
    writeln('Vetor ordenado: ');
    imprime(a, MAX);
end.
