Program notas10_while;
const N_alunos=10;
Var 
    vetor: array[1..N_alunos] of Real;
    soma, media: real;
    i, cont: Longint;
Begin
    soma := 0;
    i := 1;
    while i <= N_alunos do
    Begin
        read(vetor[i]);
        soma := soma + vetor[i];
        i := i + 1;
    End;
    media := soma/N_alunos;
    cont:= 0;
    i := 1;
    while i <= N_alunos do
    Begin
        if (vetor[i] < media) then
            cont:= cont + 1;
        i := i + 1;
    End;
    writeln('Media da turma: ',media:2:2,', ',cont,' aluno(s) ficaram abaixo');
End.

