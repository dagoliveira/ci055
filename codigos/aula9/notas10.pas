Program notas10;
const N_alunos=10;
Var 
    vetor: array[1..N_alunos] of Real;
    soma, media: real;
    i, cont: Longint;
Begin
    soma := 0;
    For i:=1 to N_alunos do
    Begin
        read(vetor[i]);
        soma := soma + vetor[i];
    End;
    media := soma/N_alunos;
    cont:= 0;
    For i:=1 to N_alunos do
    Begin
        if (vetor[i] < media) then
            cont:= cont + 1;
    End;
    writeln('Media da turma: ',media:2:2,', ',cont,' aluno(s) ficaram abaixo');
End.

