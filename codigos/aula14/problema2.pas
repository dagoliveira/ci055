program funcoes_vetor;
const MAX=20;
type vetor=array[1..MAX] of longint;

(*Variaveis globais*)
Var A,B,C: vetor;
    i, tamBC: longint;


(*Le dados e coloca num vetor 'v' ate que o usuario entre com zero;
retorna o tamanho do vetor, ou seja, quantos numeros foram lidos*)
function le_vetor(var v:vetor): longint;
var i:longint;
begin
    i := 0;
    repeat
        i := i + 1;
        read(v[i]);
    until v[i] = 0;
    le_vetor := i - 1; (*retorna tamanho do vetor*)
end;

(*Retorna verdadeiro se elemento existe no vetor*)
function busca(var v:vetor; tamanho, elemento: longint): boolean;
var i: Longint;
begin
    i:=1;
    while ( V[i] <> elemento) and (i <= tamanho) do
        i := i + 1;

    if i <= tamanho then
        busca := true
    else
        busca := false;
end;

(*Retorna quantas vezes um elemento aparece no vetor*)
function contar(var v:vetor; tamanho, elemento: longint): longint;
var i, cont: Longint;
Begin
    cont := 0;
    for i:= 1 to tamanho do
        if v[i] = elemento then
            cont := cont + 1;
    contar := cont;
end;


Begin
    randomize; (*Uma vez somente no programa*)
    for i := 1 to MAX do
    begin
         A[i] := Random(10); (*Gera numero aleatorio entre 0 e 99*)
    end;

    writeln('Vetor:');
    for i:= 1 to MAX do
        write(A[i],' ');
    writeln('');

    tamBC := 0;
    for i:=1 to MAX do
        if not(busca(B, tamBC, A[i])) then 
        begin
            tamBC := tamBC + 1;
            B[tamBC] := A[i];
            C[tamBC] := contar(A, MAX, A[i]);
        end;


    writeln('Resposta:');
    for i:= 1 to tamBC do
        writeln(B[i],' : ', C[i]);


End.
