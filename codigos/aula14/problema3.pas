program funcoes_vetor;
const MAX=200;
type vetor=array[1..MAX] of longint;

(*Variaveis globais*)
Var A,B,C: vetor;
    i, tamA, tamB, tamC: longint;


(*Le dados e coloca num vetor 'v' ate que o usuario entre com zero;
retorna o tamanho do vetor, ou seja, quantos numeros foram lidos*)
function le_vetor(var v:vetor): longint;
var i:longint;
begin
    i := 0;
    repeat
        i := i + 1;
        read(v[i]);
    until v[i] = 0;
    le_vetor := i - 1; (*retorna tamanho do vetor*)
end;

(*Retorna verdadeiro se elemento existe no vetor*)
function busca(var v:vetor; tamanho, elemento: longint): boolean;
var i: Longint;
Begin
    i:=1;
    while ( V[i] <> elemento) and (i <= tamanho) do
        i := i + 1;

    if i <= tamanho then
        busca := true
    else
        busca := false;
end;

procedure imprime_vetor(var v:vetor; tamanho:longint);
var i:longint;
begin
    for i:= 1 to tamanho do
        write(v[i],' ');
    writeln('');
end;

Begin
    writeln('Dados para o primeiro vetor, digite 0 para terminar:');
    tamA := le_vetor(A);
    writeln('Dados para o segundo vetor, digite 0 para terminar:');
    tamB := le_vetor(B);

    tamC := 0;
    for i:= 1 to tamA do
        (*Se elemento A[i] esta em B e ainda nao esta em C, adiciona em C*)
        if busca(B, tamB, A[i]) and not(busca(C, tamC, A[i])) then
        begin
            tamC := tamC + 1;
            C[tamC] := A[i];
        end;

    for i:= 1 to tamB do
        (*Se elemento B[i] esta em A e ainda nao esta em C, adiciona em C*)
        if busca(A, tamA, B[i]) and not(busca(C, tamC, B[i])) then
        begin
            tamC := tamC + 1;
            C[tamC] := B[i];
        end;


    writeln('Primeiro vetor:');
    imprime_vetor(A, tamA);
    writeln('Segundo vetor:');
    imprime_vetor(B, tamB);
    writeln('Resposta:');
    imprime_vetor(C, tamC);


End.
