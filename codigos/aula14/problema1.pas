program problema1;
type vetor=array[0..9] of longint;
Var v:vetor;
a, i: longint;

(* Para cada digito de 'a', incrementa de um a posicao no 
vetor 'v' que corresponde ao digito; 
'v' eh um vetor passado por referencia *)
procedure contabiliza_digitos(var v : vetor; a : longint);
var digito:longint;
begin
    while a > 0 do
    begin
        digito := a mod 10; (*pega o ultimo digito*)
        v[digito] := v[digito] + 1;
        a := a div 10; (*remove o ultimo digito*)
    end;
end;

procedure imprime_vetor(var v: vetor);
begin
    for i:=0 to 9 do
        writeln('[',i,'] = ',v[i],' vezes');
end;

begin
    (*Inicializa vetor com zeros*)
    for i:= 0 to 9 do
        v[i] := 0;

    (*Le varios numeros ate que o usuario entre com um zero*)
    read(a);
    while a <> 0 do
    begin
        contabiliza_digitos(v, a);
        read(a);
    end;

    (*Imprime resultado*)
    imprime_vetor(v);
end.
