Program digitos;
Var a, cont: Longint;
Begin
    read(a);
    cont := 0;
    while a <> 0 do
    begin
        cont := cont + 1;
        a := Trunc(a / 10);
    end;
    writeln(cont);
End.

