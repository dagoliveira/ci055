Program fatorial;
Var n, fat: Longint;
Begin
    read(n);
    fat := 1;
    while n >= 1 do
    begin
        fat := fat * n;
        n := n - 1;
    end;
    writeln(fat);
End.
