Program somadigitos;
Var a, soma: Longint;
Begin
    read(a);
    soma := 0;
    while a <> 0 do
    begin
        soma := soma + (a mod 10);
        a := Trunc(a / 10);
    end;
    writeln(soma);
End.

