Program tabuada;
Var i, j: Longint;
Begin
    i := 1;
    while i <= 10 do
    begin
        j := 1;
        while j <= 10 do
        begin
            writeln(i:2,' X ',j:2,' = ',i*j); (*':2' para usar duas casas ao imprimir o numero na tela, mesmo que ele tenha apenas um digito*)
            j := j + 1;
        end;
        writeln;
        i := i + 1;
    end;
End.
