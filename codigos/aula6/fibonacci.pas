Program fibonacci;
Var ultimo, penultimo, soma, cont: Longint;
Const max=20;
Begin
    ultimo := 1;
    penultimo := 1;
    cont := 3;
    write(penultimo,' ',ultimo,' ');
    while cont <= max do
    begin
        soma := ultimo + penultimo;
        write(soma,' ');
        penultimo := ultimo;
        ultimo := soma;
        cont := cont + 1;
    end;
End.

