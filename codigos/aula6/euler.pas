Program euler;
Var e: Real;
    i, fat: Longint;
Const numtermos=15; (* Constante, Pascal nao permite que seja alterada no programa*)
Begin
    e := 0;
    fat := 1;
    i := 1;
    while i <= numtermos do
    begin
        e := e + 1/fat;
        fat := fat * i;
        i := i + 1;
    end;
    writeln('e = ',e);
End.

