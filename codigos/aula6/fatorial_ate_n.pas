Program fatorial_ate_n;
Var i, n, fat: Longint;
Begin
    read(n);
    fat := 1;
    i := 1;
    while i <= n do
    begin
        fat := fat * i;
        writeln(i:3,'!: ',fat);
        i := i + 1;
    end;
End.

