Program primos_funcao;
Const N=100;
Var a: Longint;

function primo(a: Longint): Boolean;
var i: Longint;
Begin
    primo := true; {retorno da funcao inicialmente eh true}
    for i:=2 to Trunc(sqrt(a)) do
        if (a mod i) = 0 then
            primo := false; {altera retorno para false}
End;

Begin
    for a := 2 to N do
        if primo(a) then
            writeln(a);
End.
