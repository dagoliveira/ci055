Program fatorial;
Var i: Longint; {Variável global}

function fatorial(n: Longint): Longint;
var fat: Longint; {Variável local}
Begin
    fat := 1;
    while n >= 1 do
    begin
        fat := fat * n;
        n := n - 1;
    end;
    fatorial := fat; {Retorno da funcao, usa-se o nome dela}
End;

Begin {codigo principal fica simples e legivel}
    for i:=1 to 10 do
        writeln(fatorial(i));
End.
