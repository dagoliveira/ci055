program troca;
Var a: Array[1..10] of Longint;
    i: longint;

procedure troca(var a,b: longint);
var temp: longint;
begin
    temp := a;
    a := b;
    b := temp;
end;

Begin
    for i:= 1 to 10 do {Inicia o vetor}
        a[i] := i;
    for i:= 1 to 10 do {Imprime o vetor}
        write(a[i]:3);

    troca(a[2],a[8]); {troca duas posicoes}
    writeln();

    for i:= 1 to 10 do {Imprime o vetor}
        write(a[i]:3);
End.
