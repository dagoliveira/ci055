program troca_nao_funciona;
Var a,b: Longint;

procedure troca(a,b: longint);
var temp: longint;
begin
    temp := a;
    a := b;
    b := temp;
end;

Begin
    read(a,b);
    writeln('a: ',a,', b: ',b);
    troca(a,b);
    writeln('a: ',a,', b: ',b);
End.
