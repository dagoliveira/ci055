Program fatorial;
Var i,n, fat: Longint;
Begin
    for i:=1 to 10 do
    begin
        fat := 1;
        n := i;
        while n >= 1 do
        begin
            fat := fat * n;
            n := n - 1;
        end;
        writeln(fat);
    end;
End.
