Program fatorial;
Var i, fat: Longint; {Variaveis globais}

procedure fatorial(n: Longint);
Begin
    fat := 1;
    while n >= 1 do
    begin
        fat := fat * n;
        n := n - 1;
    end;
End;

Begin {codigo (bloco) principal}
    for i:=1 to 10 do
    begin
        fatorial(i);
        writeln(fat);{variavel fat foi alterada no procedimento fatorial}
    end;
End.
