program troca;
type tipo_vetor=Array[1..10] of Longint; {tipo_vetor define um vetor de tamanho 10}
Var i: longint;
    a : tipo_vetor; {vetor de tamanho 10}

{lembrem de passar o vetor como referência, usando 'var'}
procedure troca(var v: tipo_vetor; a,b: longint); 
var temp: longint;
begin
    temp := v[a];
    v[a] := v[b];
    v[b] := temp;
end;

Begin
    for i:= 1 to 10 do {Inicia o vetor}
        a[i] := i;
    for i:= 1 to 10 do {Imprime o vetor}
        write(a[i]:3);

    troca(a,8,2); {troca duas posicoes, informa o nome do vetor e os indices}
    writeln();
    for i:= 1 to 10 do {Imprime o vetor}
        write(a[i]:3);
End.
