program troca_nao_funciona_2;
Var a,b: Longint;

procedure troca(a: Longint; var b: Longint);
var temp: Longint;
begin
    temp := a;
    a := b;
    b := temp;
end;

Begin
    read(a,b);
    writeln('a: ',a,', b: ',b);
    troca(a,b);
    writeln('a: ',a,', b: ',b);
End.
