program expressao_balanceada;
const max=100;
type
    elemento = longint;
    pilha = record
        ini : longint;
        conteudo : array[1..max] of elemento;
    end;

procedure inicializa_pilha(var p: pilha);
(*
    Inicializa a estrutura
*)
begin
    p.ini := 0;
end;


function pilha_vazia(var p: pilha): boolean;
(*
    Retorna true se a pilha esta vazia
*)
begin
    if p.ini > 0 then
        pilha_vazia := false
    else
        pilha_vazia := true
end;

procedure empilha(x:elemento; var p: pilha);
(*
    Insere x no inicio da pilha
*)
begin
    p.ini := p.ini + 1;
    p.conteudo[p.ini] := x;
end;

function desempilha(var p: pilha): elemento;
(*
    Retorna o elemento do inicio da pilha e remove dela
*)
begin
    if p.ini > 0 then
    begin
        desempilha := p.conteudo[p.ini];
        p.ini := p.ini - 1;
    end
    else
        desempilha := 0;
end;

function topo(var p: pilha): elemento;
(*
    Retorna o elemento do inicio da pilha, sem remove-lo
*)
begin
    if p.ini > 0 then
    begin
        topo := p.conteudo[p.ini];
    end
    else
        topo := 0;
end;



var 
    p1: pilha;
    a,b: elemento;
    pares_ok: boolean;
begin
    inicializa_pilha(p1); 
    pares_ok := true;

    read(a);
    while a <> 0 do
    begin
        if a > 0 then
            empilha(a, p1)
        else
        begin
            if pilha_vazia(p1) then
                pares_ok := false
            else
            begin
                b := desempilha(p1);
                if b <> -a then
                    pares_ok := false;
            end;
        end;

        read(a);
    end;


    if pares_ok and pilha_vazia(p1) then
        writeln('SIM')
    else
        writeln('NAO');

end.
