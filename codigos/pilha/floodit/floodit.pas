program floodit_game;
uses Crt, tadpilha;
(*
  Black = 0;
  Blue = 1;
  Green = 2;
  Cyan = 3;
  Red = 4;
  Magenta = 5;
  Brown = 6;
  LightGray = 7;
  DarkGray = 8;
  LightBlue = 9;
  LightGreen = 10;
  LightCyan = 11;
  LightRed = 12;
  LightMagenta = 13;
  Yellow = 14;
  White = 15;
*)
const 
    maxMat=14; (* Tamanho da matriz do jogo *)
    maxColor=6; (* Quantidade de cores *)
    maxJogadas=25; (* Quantidade maxima permitida de jogadas*)
type matriz=array[1..maxMat,1..maxMat] of longint;


procedure imprime_matriz(var mat: matriz);
(* Imprime na tela uma matriz com max linhas e max colunas *)
var i,j: longint;
begin
    for i:=1 to maxMat do
    begin
        for j:=1 to maxMat do
        begin
            TextBackground(mat[i,j]);
            write(mat[i,j]:2, ' ');
            TextBackground(Black);
        end;
        writeln('');
    end;
end;

procedure gera_matriz(var mat: matriz);
(* Gera uma matriz nova usando numeros aleatorios *)
var i,j: longint;
begin
    randomize;
    for i:=1 to maxMat do
    begin
        for j:=1 to maxMat do
        begin
            (*Gera um numero aleatorio entre 1 e 'maxColor' *)
            mat[i,j] := Random(maxColor)+1; 
        end;
    end;
end;


var
    flood_mat : matriz;
    jogadas,nova_cor: longint;
    
begin
    gera_matriz(flood_mat);
    jogadas := 0;
    nova_cor := 1;
    while (jogadas < maxJogadas) and (nova_cor <> 0) (*and NaoGanhouJogo *) do
    begin
        ClrScr;
        writeln('Jogada: ',jogadas,'/',maxJogadas);
        imprime_matriz(flood_mat);

        write('Entre com uma nova cor (entre 1 e ',maxColor,'), ou 0 para terminar: ');
        read(nova_cor);
        jogadas := jogadas+1;

        (* Atualizar a matriz com a nova cor *)

        (* Verificar se o jogo nao foi completado, ou seja, toda a matriz tem apenas uma cor*)
    end;

    writeln('Fim de jogo');
end.
