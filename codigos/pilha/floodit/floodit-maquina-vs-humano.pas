program floodit_game;
uses Crt, tadpilha;
(*
  Black = 0;
  Blue = 1;
  Green = 2;
  Cyan = 3;
  Red = 4;
  Magenta = 5;
  Brown = 6;
  LightGray = 7;
  DarkGray = 8;
  LightBlue = 9;
  LightGreen = 10;
  LightCyan = 11;
  LightRed = 12;
  LightMagenta = 13;
  Yellow = 14;
  White = 15;
*)
const 
    maxMat=14; (* Tamanho da matriz do jogo *)
    maxColor=6; (* Quantidade de cores *)
    maxJogadas=100; (* Quantidade maxima permitida de jogadas*)
    tempo_entre_jogadas=500; (* Tempo em ms *)
type matriz=array[1..maxMat,1..maxMat] of longint;


procedure imprime_matriz(var mat: matriz);
(* Imprime na tela uma matriz com max linhas e max colunas *)
var i,j: longint;
begin
    for i:=1 to maxMat do
    begin
        for j:=1 to maxMat do
        begin
            TextBackground(mat[i,j]);
            write(mat[i,j]:2, ' ');
            TextBackground(Black);
        end;
        writeln('');
    end;
end;

procedure gera_matriz(var mat: matriz);
(* Gera uma matriz nova usando numeros aleatorios *)
var i,j: longint;
begin
    for i:=1 to maxMat do
    begin
        for j:=1 to maxMat do
        begin
            (*Gera um numero aleatorio entre 1 e 'maxColor' *)
            mat[i,j] := Random(maxColor)+1; 
        end;
    end;
end;

procedure copia_matriz(var mat_fonte, mat_destino: matriz);
var i,j: longint;
begin
    for i:=1 to maxMat do
    begin
        for j:=1 to maxMat do
        begin
            mat_destino[i,j] := mat_fonte[i,j]; 
        end;
    end;
end;

function ganhou_jogo(var mat: matriz): boolean;
(*
    Retorna verdadeiro (True) se o jogo foi ganho, ou seja, toda a matriz tem apenas uma cor;
    Retorna falso (False) caso contrario
*)
var i,j,cor: longint;
begin
    ganhou_jogo := True;
    cor := mat[1,1];
    for i:=1 to maxMat do
    begin
        for j:=1 to maxMat do
        begin
            if mat[i,j] <> cor then
                ganhou_jogo := False;
        end;
    end;

end;

function testar_vizinho(var fmat : matriz; i,j,cor:longint): boolean;
(*
    Verifica se elemento [i,j] eh uma posicao valida na matriz e se tem a cor desejada
*)
begin
    if (i < 1) or (i > maxMat) or (j < 1) or (j > maxMat) then
        testar_vizinho := false
    else
    begin
        if fmat[i,j] = cor then
            testar_vizinho := true
        else
            testar_vizinho := false;
    end;
end;

function busca_pilha(pilha : TAD_Pilha; i,j:longint): boolean;
(*
    Busca se um elemento (i,j) esta numa pilha. 
    Como a pilha eh passada por 'copia', ou seja, sem o uso do 'var'; Podemos remover todos os elementos sem afetar a outra pilha usada na chamada da funcao
*)
var k,l:longint;
begin
    busca_pilha := false;
    while tamanho_pilha(pilha) > 0 do
    begin
        remover_pilha(pilha,k,l);
        if (k = i) and (l = j) then
            busca_pilha := true;
    end;
end;

procedure flood(var fmat : matriz; nova_cor : longint);
(*
    Busca por todos os vizinhos de [1,1] que tem a mesma cor que ele, e substitui pela nova cor
*)
var 
    P_verificar, P_mudar: TAD_Pilha;
    i,j,cor_atual: longint;
begin

    (* Inicializa as pilhas *)
    inicializar_pilha(P_verificar);
    inicializar_pilha(P_mudar);

    (* Salva a cor do elemento [1,1] numa variavel auxiliar *)
    cor_atual := fmat[1,1];

    (* Insere o elemento [1,1] na pilha que usaremos para alterar as cor *)
    inserir_pilha(P_mudar, 1, 1);

    (* Verifica se os vizinhos de [1,1] tem a mesma cor que ele, e insere na pilha de elementos que precisam ser verificados *)
    if fmat[1,2] = cor_atual then
        inserir_pilha(P_verificar, 1, 2);
    if fmat[2,1] = cor_atual then
        inserir_pilha(P_verificar, 2, 1);

    (* Percorre a pilha de elementos que precisam ser verificados*)
    while tamanho_pilha(P_verificar) > 0 do
    begin
        (* Retira um elemento que precisa ser verificado, e olha seus vizinhos *)
        remover_pilha(P_verificar,i,j);
        (* Insere esse elemento na pilha de elementos que usaremos para alterar a cor*)
        inserir_pilha(P_mudar, i, j);

        (* Para cada vizinho do elemento que estamos verificando, testamos se ele tem a mesma cor e se nao 
        existe na pilha de elementos que usaremos para alterar a cor *)
        if testar_vizinho(fmat,i-1,j,cor_atual) and not( busca_pilha(P_mudar,i-1,j) ) then
            inserir_pilha(P_verificar, i-1, j);
        if testar_vizinho(fmat,i,j-1,cor_atual) and not( busca_pilha(P_mudar,i,j-1) ) then
            inserir_pilha(P_verificar, i, j-1);
        if testar_vizinho(fmat,i+1,j,cor_atual) and not( busca_pilha(P_mudar,i+1,j) ) then
            inserir_pilha(P_verificar, i+1, j);
        if testar_vizinho(fmat,i,j+1,cor_atual) and not( busca_pilha(P_mudar,i,j+1) ) then
            inserir_pilha(P_verificar, i, j+1);
    end;

    (* Percorre a pilha com os elementos que precisam ter a cor alterada *)
    while tamanho_pilha(P_mudar) > 0 do
    begin
        remover_pilha(P_mudar,i,j);
        fmat[i,j] := nova_cor;
    end;
end;

function nova_jogada(var fmat: matriz): longint;
(*
    Funcao que escolhe uma nova cor para a proxima jogada
*)
begin
    (* Ignora a matriz com as cores e faz uma jogada ingenua, escolhe aleatoriamente uma cor *)
    nova_jogada := Random(maxColor) + 1;

    (* Faca uma jogada melhor do que a aleatoria!!!!! *)
end;

var
    flood_mat, flood_mat_humano : matriz;
    jogadas,nova_cor_maq,nova_cor: longint;
    
begin
    randomize;
    gera_matriz(flood_mat);
    copia_matriz(flood_mat, flood_mat_humano);

    jogadas := 0;
    nova_cor := 1;

    ClrScr;
    writeln('Jogada: ',jogadas,'/',maxJogadas);
    writeln('Matriz maquina: ');
    imprime_matriz(flood_mat);
    writeln('Matriz humano: ');
    imprime_matriz(flood_mat_humano);
    writeln('Fim de jogo');
    while (jogadas < maxJogadas) and (nova_cor <> 0) and not( ganhou_jogo(flood_mat) ) and not( ganhou_jogo(flood_mat_humano) ) do
    begin

        (* Jogada da maquina *)
        nova_cor_maq := nova_jogada(flood_mat);
        flood(flood_mat, nova_cor_maq);


        (* Jogada do humano *)
        write('Entre com uma nova cor (entre 1 e ',maxColor,'), ou 0 para terminar: ');
        read(nova_cor);
        flood(flood_mat_humano, nova_cor);

        jogadas := jogadas+1;
        ClrScr;
        writeln('Jogada: ',jogadas,'/',maxJogadas);
        writeln('Maquina escolheu a cor ',nova_cor_maq);
        writeln('Matriz maquina: ');
        imprime_matriz(flood_mat);
        writeln('Matriz humano: ');
        imprime_matriz(flood_mat_humano);
    end;

    if ganhou_jogo(flood_mat) and not( ganhou_jogo(flood_mat_humano) ) then
    begin
        writeln('Sinto muito, voce perdeu para a maquina!');
    end
    else if ganhou_jogo(flood_mat_humano) and not( ganhou_jogo(flood_mat) ) then
    begin
        writeln('Parabens, voce ganhou da maquina!');
    end
    else if ganhou_jogo(flood_mat_humano) and ganhou_jogo(flood_mat) then
    begin
        writeln('Ops, chegamos a um empate!');
    end
    else if nova_cor <> 0 then
        writeln('Se serve de consolo, nem a maquina ganhou!');
end.
