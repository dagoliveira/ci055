program pilha_basico;
uses tadpilha;
(*

A biblioteca 'tadpilha' disponibiliza o tipo pila:
    TAD_pilha

e as seguintes funcoes e procedimento:

inicializa uma pilha:
    procedure inicializar_pilha(var pilha: TAD_pilha);

informa quantos elementos tem na pilha:
    function tamanho_pilha(var pilha: TAD_pilha): longint;

insere uma coordenada (lin,col) na pilha:
    procedure inserir_pilha(var pilha: TAD_pilha; lin,col: longint);

remove a coordenada no topo da pilha e salva nas variaveis 'lin' e 'col':
    procedure remover_pilha(var pilha: TAD_pilha; var lin,col: longint);

retorna a coordenada no topo da pilha sem remove-la da pilha,
a coordenada eh salva nas variaveis 'lin' e 'col':
    procedure topo_pilha(var pilha: TAD_pilha; var lin,col: longint);


Exemplo de uso:
var 
    p: TAD_pilha; => cria uma variavel do tipo pilha
    l,c: longint;
begin
    inicializar_pilha(p); => inicializa a pilha para poder usa-la
    l:=2;c:=3;
    inserir_pilha(p, l, c); => inseriu o conteudo de 'l' e 'c' na pilha 'p'
    inserir_pilha(p, 5, 3); => inseriu '5' e '3' na pilha 'p'
    remover_pilha(p, l, c); => agora 'l' tem o valor 5 e 'c' o valor 3
end;
*)

const 
    maxLinha=30; (* Numero maximo de linhas na matriz *)
    maxColuna=80; (* Numero Maximo de colunas na matriz *)

type

    (* Uma matriz de 'maxLinha x maxColuna'*)
    matriz = array[1..maxLinha,1..maxColuna] of char;

procedure ler_labirinto(var mat: matriz);
(* 
    Faz a leitura de um labirinto para a matriz 'mat' 
*)
var i,j:longint;
    c:char;
begin
    for i:=1 to maxLinha do
    begin
        for j:=1 to maxColuna do
            read(mat[i,j]);
        read(c); (* Removendo o caracter 'fim de linha' *)
    end;
end;

procedure imprimir_labirinto(var mat: matriz);
(* 
    Imprime na tela um labirinto representado pela matriz 'mat' 
*)
var i,j:longint;
begin
    for i:=1 to maxLinha do
    begin
        for j:=1 to maxColuna do
        begin
            write(mat[i,j]);
        end;
        writeln('');
    end;
    writeln('');
end;



procedure busca_posicao_inicial(var lab: matriz; var lin,col: longint);
var i,j: longint;
begin
    for i := 1 to maxLinha do
        for j := 1 to maxColuna do
            if lab[i,j] = '+' then
            begin
                lin := i;
                col := j;
            end;
end;

procedure copiar_matriz(var m1, m2: matriz);
var i,j : longint;
begin
    for i := 1 to maxLinha do
        for j := 1 to maxColuna do
            m2[i,j] := m1[i,j];
end;



(*
    Procedimento que deve ser implementado pelo aluno
    Deve-se achar a saida do labirinto e substituir na matriz 'lab' o caminho encontrado pelo caracter '+'
*)
procedure resolve_labirinto(var lab: matriz);
var 
    pilha: TAD_pilha;
    lab_copia : matriz;
    lin,col: longint;
begin

    inicializar_pilha(pilha);

    copiar_matriz(lab,lab_copia);

    busca_posicao_inicial(lab,lin,col);
    inserir_pilha(pilha,lin,col);

    while ((lin > 1) and (lin < maxLinha)) and ((col > 1) and (col < maxColuna)) and (tamanho_pilha(pilha) > 0) do
    begin
        (* Tenta ir para cima *)
        if lab_copia[lin-1, col] = ' ' then
        begin
            lin:=lin-1;
            inserir_pilha(pilha,lin,col);
            lab_copia[lin, col] := '|';
        end
        (* Tenta ir para esquerda *)
        else if lab_copia[lin, col-1] = ' ' then
        begin
            col:=col-1;
            inserir_pilha(pilha,lin,col);
            lab_copia[lin, col] := '|';
        end
        (* Tenta ir para direita *)
        else if lab_copia[lin, col+1] = ' ' then
        begin
            col:=col+1;
            inserir_pilha(pilha,lin,col);
            lab_copia[lin, col] := '|';
        end
        (* Tenta ir para baixo *)
        else if lab_copia[lin+1, col] = ' ' then
        begin
            lin:=lin+1;
            inserir_pilha(pilha,lin,col);
            lab_copia[lin, col] := '|';
        end
        (* Nao tem como ir para lugar nenhum, remove a posicao da pilha e volta analisar a anterior *)
        else 
        begin
            remover_pilha(pilha,lin,col); (* remove posicao que esta num beco sem saida *)
            topo_pilha(pilha,lin,col); (* vola a analisar posicao anterior, que agora esta no topo da pilha *)
        end;
    end;

    (* 
        Coloca um '+' nas posicoes do caminho encontrado, 
        para indicar no labirinto original qual eh o caminho 
    *)
    while (tamanho_pilha(pilha) > 0) do
    begin
        remover_pilha(pilha,lin,col);
        lab[lin,col] := '+';
    end;

end;

(*
    Programa principal comeca aqui
*)
var (* Variaveis usadas no programa principal*)
    labirinto: matriz;
begin
    (* Faz a leitura do labirinto *)
    ler_labirinto(labirinto); 

    (* Procedimento que deve ser implementado pelo aluno, esse procedimento deve achar a saida do labirinto *)
    resolve_labirinto(labirinto);

    (* Imprime o labirinto na tela *)
    imprimir_labirinto(labirinto);
end.

