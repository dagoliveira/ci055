unit tadpilha;

interface

const 
    maxPilha=10000; (* Tamanho maximo da pilha *)
type
    (* Pilha que armazena coordenadas: linha e coluna de uma matriz *)
    TAD_pilha = record
        topo : longint;
        linha  : array[1..maxPilha] of longint; 
        coluna : array[1..maxPilha] of longint; 
    end;


procedure inicializar_pilha(var pilha: TAD_pilha);
procedure inserir_pilha(var pilha: TAD_pilha; lin,col: longint);
function tamanho_pilha(var pilha: TAD_pilha): longint;
procedure remover_pilha(var pilha: TAD_pilha; var lin,col: longint);
procedure topo_pilha(var pilha: TAD_pilha; var lin,col: longint);

implementation

procedure inicializar_pilha(var pilha: TAD_pilha);
(*
    Inicia uma pilha garantindo que os valores iniciais estarao corretos.
    A pilha deve ser iniciada com topo 'zero'
*)
begin
    pilha.topo := 0;
end;

procedure inserir_pilha(var pilha: TAD_pilha; lin,col: longint);
(*
    Insere uma coordenada na pilha
*)
begin
    pilha.topo := pilha.topo + 1;
    pilha.linha[pilha.topo] := lin;
    pilha.coluna[pilha.topo] := col;
end;

function tamanho_pilha(var pilha: TAD_pilha): longint;
(*
    Retorna quantos elementos estao inseridos na pilha
*)
begin
    tamanho_pilha := pilha.topo;
end;

procedure remover_pilha(var pilha: TAD_pilha; var lin,col: longint);
(*
    Retorna a coordenada no topo da pilha e o remove da pilha, ou retorna uma coordenada (0,0)
    O retorno vai ser pelas variaveis 'lin' e 'col'
*)
begin
    if pilha.topo > 0 then
    begin
        lin := pilha.linha[pilha.topo];
        col := pilha.coluna[pilha.topo];
        pilha.topo := pilha.topo - 1;
    end
    else
    begin
        lin := 0;
        col := 0;
    end;
end;

procedure topo_pilha(var pilha: TAD_pilha; var lin,col: longint);
(*
    Retorna a coordenada no topo da pilha sem remove-la, ou retorna uma coordenada (0,0) se a pilha estiver vazia
    O retorno vai ser pelas variaveis 'lin' e 'col'
*)
begin
    if pilha.topo > 0 then
    begin
        lin := pilha.linha[pilha.topo];
        col := pilha.coluna[pilha.topo];
    end
    else
    begin
        lin := 0;
        col := 0;
    end;
end;

end.
