program pilha_basico;
uses tadpilha;
(*

A biblioteca 'tadpilha' disponibiliza o tipo pila:
    TAD_pilha

e as seguintes funcoes e procedimento:

inicializa uma pilha:
    procedure inicializar_pilha(var pilha: TAD_pilha);

informa quantos elementos tem na pilha:
    function tamanho_pilha(var pilha: TAD_pilha): longint;

insere uma coordenada (lin,col) na pilha:
    procedure inserir_pilha(var pilha: TAD_pilha; lin,col: longint);

remove a coordenada no topo da pilha e salva nas variaveis 'lin' e 'col':
    procedure remover_pilha(var pilha: TAD_pilha; var lin,col: longint);

retorna a coordenada no topo da pilha sem remove-la da pilha,
a coordenada eh salva nas variaveis 'lin' e 'col':
    procedure topo_pilha(var pilha: TAD_pilha; var lin,col: longint);


Exemplo de uso:
var 
    p: TAD_pilha; => cria uma variavel do tipo pilha
    l,c: longint;
begin
    inicializar_pilha(p); => inicializa a pilha para poder usa-la
    l:=2;c:=3;
    inserir_pilha(p, l, c); => inseriu o conteudo de 'l' e 'c' na pilha 'p'
    inserir_pilha(p, 5, 3); => inseriu '5' e '3' na pilha 'p'
    remover_pilha(p, l, c); => agora 'l' tem o valor 5 e 'c' o valor 3
end;
*)

const 
    maxLinha=30; (* Numero maximo de linhas na matriz *)
    maxColuna=80; (* Numero Maximo de colunas na matriz *)

type

    (* Uma matriz de 'maxLinha x maxColuna'*)
    matriz = array[1..maxLinha,1..maxColuna] of char;

procedure ler_labirinto(var mat: matriz);
(* 
    Faz a leitura de um labirinto para a matriz 'mat' 
*)
var i,j:longint;
    c:char;
begin
    for i:=1 to maxLinha do
    begin
        for j:=1 to maxColuna do
            read(mat[i,j]);
        read(c); (* Removendo o caracter 'fim de linha' *)
    end;
end;

procedure imprimir_labirinto(var mat: matriz);
(* 
    Imprime na tela um labirinto representado pela matriz 'mat' 
*)
var i,j:longint;
begin
    for i:=1 to maxLinha do
    begin
        for j:=1 to maxColuna do
        begin
            write(mat[i,j]);
        end;
        writeln('');
    end;
    writeln('');
end;


(*
    Procedimento que deve ser implementado pelo aluno
    Deve-se achar a saida do labirinto e substituir na matriz 'lab' o caminho encontrado pelo caracter '+'
*)
procedure resolve_labirinto(var lab: matriz);
begin

end;

(*
    Programa principal comeca aqui
*)
var (* Variaveis usadas no programa principal*)
    labirinto: matriz;
begin
    (* Faz a leitura do labirinto *)
    ler_labirinto(labirinto); 

    (* Procedimento que deve ser implementado pelo aluno, esse procedimento deve achar a saida do labirinto *)
    resolve_labirinto(labirinto);

    (* Imprime o labirinto na tela *)
    imprimir_labirinto(labirinto);
end.

