program pilha_basico;
const max=1000;
type
    T_elemento = longint;
    TAD_pilha = record
        tamanho : longint;
        conteudo : array[1..max] of T_elemento;
    end;

procedure inicializar_pilha(var pilha: TAD_pilha);
(*
    Inicia uma pilha garantindo que os valores iniciais estarao corretos.
    A pilha deve ser iniciada com tamanho 'zero', o vetor de conteudo pode ser ignorado
*)
begin
    pilha.tamanho := 0;
end;

procedure inserir_pilha(var pilha: TAD_pilha; elemento: T_elemento);
(*
    Inseri um elemento na pilha
*)
begin
    pilha.tamanho := pilha.tamanho + 1;
    pilha.conteudo[pilha.tamanho] := elemento;
end;

function tamanho_pilha(var pilha: TAD_pilha): longint;
(*
    Retorna o tamanho atual da pilha, ou seja, quanto elementos estao inseridos
*)
begin
    tamanho_pilha := pilha.tamanho;
end;

function remover_pilha(var pilha: TAD_pilha): T_elemento;
(*
    Retorna o elemento no topo da pilha e o remove da pilha
*)
begin
    if pilha.tamanho > 0 then
    begin
        remover_pilha := pilha.conteudo[pilha.tamanho];
        pilha.tamanho := pilha.tamanho - 1;
    end
    else
        remover_pilha := 0;
end;

function topo_pilha(var pilha: TAD_pilha): T_elemento;
(*
    Retorna o elemento no topo da pilha sem remove-lo
*)
begin
    if pilha.tamanho > 0 then
    begin
        topo_pilha := pilha.conteudo[pilha.tamanho];
    end
    else
        topo_pilha := 0;
end;



var 
    p1: TAD_pilha;
    i: T_elemento;
begin
    inicializar_pilha(p1); (* Inicia a pilha 'p1' *)

    repeat  
        read(i);
        inserir_pilha(p1, i);
    until i = 0; (* Faz a leitura de varios numeros ate que entre com o 'zero' *)

    remover_pilha(p1); (* Remove ultimo zero entrado *)

    writeln('Imprimindo os numeros da pilha, como estao numa pilha a ordem sera reversa: ');
    while tamanho_pilha(p1) > 0 do (* Remve elementos da pilha ate nao ter mais nenhum*)
        write(remover_pilha(p1), ' '); (* Vai imprimindo os elementos enquanto remove*)


    writeln('');

end.
