program senox ;
const itmax=20;
var seno, x, xquadrado, numerador, novotermo, fat : real;
     i, sinal : Longint;
begin
    seno :=  0;
    fat :=  1;
    sinal :=  1;
    read(x);
    xquadrado := x*x ;
    numerador :=  x;
    i :=  1;
    while i <= itmax do 
    begin
        novotermo :=  numerador/fat; (*atualiza  o  termo*)
        seno :=  seno +  sinal*novotermo;
        numerador :=  numerador*xquadrado; (*atualiza  o  numerador*)
        fat :=  2*i*(2*i+1)*fat; (*atualiza  o  denominador*)
        sinal := -sinal; (*inverte o sinal a cada iteracao*)
        i :=  i  +  1;
    end;
    writeln('seno(',x:3:6,')= ', seno:3:6);
end.
