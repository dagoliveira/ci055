program funcoes_vetor;
const MAX=20;
type vetor=array[1..MAX] of real;

(*Variaveis globais*)
Var A: vetor;
    i, maior_indice, menor_indice: longint;
    media_vetor: Real;

(*Retorna o indice do maior elemento*)
function maior(var V:vetor; tamanho: longint): Longint;
var i, m: Longint;
Begin
    m := 1;
    for i:=2 to tamanho do
        if V[i] > V[m] then
            m := i;

    maior := m;
End;

(*Retorna o indice do menor elemento*)
function menor(var V:vetor; tamanho: longint): Longint;
var i, m: Longint;
Begin
    m := 1;
    for i:=2 to tamanho do
        if V[i] < V[m] then
            m := i;

    menor := m;
End;

(*Retorna a media aritmetica de todos os elementos do vetor*)
function media(var V:vetor; tamanho: longint): Real;
var i: Longint; soma : Real;
Begin
    soma := 0;
    for i:=1 to tamanho do
        soma := soma + V[i];

    media := soma/tamanho;
End;


Begin
    i := 0;
    writeln('Entre com no maximo ', MAX,' numeros, digite 0 para terminar:');
    repeat
        i := i + 1;
        read(A[i]);
    until A[i] = 0;
    maior_indice := maior(A, i-1);
    menor_indice := menor(A, i-1);
    media_vetor := media(A, i-1);
    writeln('indice do maior elemento: ',maior_indice);
    writeln('indice do menor elemento: ',menor_indice);
    writeln('media do vetor: ',media_vetor:3:2);
End.
