program busca_generica;
const MAX=20;
type vetor=array[1..MAX] of real;
Var A: vetor;
    i, pos_elemento: longint;
    elem: real;

(*retorna 0 caso nao encontre o elemento, ou a posicao do elemento caso ele esteja no vetor*)
function busca(var V:vetor; elemento: real; tamanho: longint): Longint;
var i, cont: Longint;
Begin
    cont := 0;
    i:=1;
    while ( V[i] <> elemento) and (i <= tamanho) do
    begin
        cont := cont + 1;
        i := i + 1;
    end;
    if i <= tamanho then
        busca := i
    else
        busca := 0;

    writeln('Busca generica levou ', cont, ' passos');
End;


Begin
    i := 0;
    writeln('Entre com no maximo ', MAX,' numeros, digite 0 para terminar:');
    repeat
        i := i + 1;
        read(A[i]);
    until A[i] = 0;

    write('Entre com um elemento para verificar se esta no vetor: ');
    read(elem);
    pos_elemento := busca(A, elem, i-1);
    if (pos_elemento <> 0) then
        writeln('Elemento existe, esta na posicao: ',pos_elemento)
    else
        writeln('Elemento nao encontrado');
End.
