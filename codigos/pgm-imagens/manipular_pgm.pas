(* 
Como usar:
./manipular_pgm < imagem.pgm > nova-imagem.pgm

Escolha uma imagem da pasta amostras, por exemplo:
./manipular_pgm < amostras/baboon.ascii.pgm > baboon-manipulado.pgm
*)
program manipular_pgm;
const max=1000;
type matriz=array[1..max,1..max] of longint;


procedure ler_pgm(var mat: matriz; var largura,altura,intensidade_max:longint);
(* Faz a leitura de uma imagem no formato PGM *)
var i,j:longint; formato:string[20];
begin
    read(formato);
    read(largura);
    read(altura);
    read(intensidade_max);
    for i:=1 to altura do
        for j:=1 to largura do
            read(mat[i,j]);
end;

procedure imprime_pgm(var mat: matriz; largura,altura,intensidade_max:longint);
(* Faz a impressao na tela de uma imagem no formato PGM *)
var i,j: longint;
begin
    writeln('P2');
    writeln(largura, ' ', altura);
    writeln(intensidade_max);
	for i:=1 to altura do
	begin
		for j:=1 to largura do
		begin
			write(mat[i,j], ' ');
		end;
		writeln('');
	end;
end;

procedure deteccao_borda(var mat, mat2: matriz; largura,altura,limiar:longint);
var i,j: longint;
begin
   for i:=1 to altura do
   begin
      mat2[i,1]:= 0;
      mat2[i,largura]:= 0;
   end;
   for j:= 1 to largura do
   begin
      mat2[1,j]:= 0;
      mat2[altura,j]:= 0;
   end;
   for i:= 2 to altura-1 do
      for j:= 2 to largura-1 do
	 if abs(mat[i,j]*4 -(mat[i-1,j]+mat[i+1,j]+mat[i,j-1]+mat[i,j+1])) > limiar then
	    mat2[i,j]:= 255
	 else
	    mat2[i,j]:= 0
end;


var 
    imagem,saida:matriz;
    largura,altura,intensidade_max: longint;
begin
    ler_pgm(imagem, largura, altura, intensidade_max);

    deteccao_borda(imagem, saida, largura, altura, 50);
    intensidade_max := 255;

    imprime_pgm(saida, largura, altura, intensidade_max);
end.
