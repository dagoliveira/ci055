Program maior_menor;
Const MAX=10;
Var vetor: array[1..MAX] of real;
i, maior, menor: longint;
Begin
    for i:= 1 to MAX do
    begin
        read(vetor[i]);
    end;

    maior := 1;
    menor := 1;
    for i:= 2 to MAX do
    begin
        if vetor[i] > vetor[maior] then
            maior := i;
        if vetor[i] < vetor[menor] then
            menor := i
    end;

    writeln('maior: ',maior,', menor: ',menor);
End.
