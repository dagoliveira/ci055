Program primos_vetor;
Const N=1000000;
Var i,j: Longint;
vetor :array[2..N] of boolean;
Begin
    for i := 2 to N do
    begin
        vetor[i] := true;
    end;

    for i := 2 to N do
    begin
        if vetor[i] then
        begin
            j:= i + i;
            while j <= N do
            begin
                vetor[j] := false;
                j := j + i;
            end;
        end;
    end;

    for i := 2 to N do
    begin
        if vetor[i] then
            writeln(i)
    end;
End.
