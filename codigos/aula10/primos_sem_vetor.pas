Program primos_sem_vetor;
Const N=1000000;
Var a,b: Longint;
    primo: Boolean;
Begin
    a := 2;
    while a <= N do
    begin
        primo := true;
        b := 2;
        while b <= sqrt(a) do
        begin
            if (a mod b) = 0 then
                primo := false;
            b := b + 1;
        end;

        if primo then
            writeln(a);
        a := a + 1;
    end;
End.
