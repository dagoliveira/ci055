program mdcporeuclides;
var a, b, resto, cont : Longint;
begin
    read (a, b) ;
    if ((a <> 0) AND (b <> 0)) then
    begin
        resto:= a mod b;
        cont := 0;
        while resto <> 0 do
        begin
            a:= b;
            b:= resto ;
            resto:= a mod b;
            cont := cont + 1;
        end;
        writeln ('mdc = ', b);
        writeln ('while executou ',cont,' vezes');
    end
    else
        writeln ('o algoritmo nao funciona para entradas nulas.');
End.
