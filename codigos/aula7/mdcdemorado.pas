program mdcdemorado;
var a, b, cont, mdc : Longint;
begin
    read (a, b);
    cont := 1;
    while (cont < a) and (cont < b) do
    begin
        if (a mod cont = 0) and (b mod cont = 0) then
            mdc := cont;
        cont := cont + 1;
    end;

    writeln('mdc = ', mdc);
    writeln('while executou ',cont,' vezes');
End.
