Program strong;
Uses math;
Var a, num, soma, fat, cont: Longint;
Begin
    read(a);

    soma := 0;
    num := a;
    while num > 0 do
    begin
        cont := num mod 10;
        fat := 1;
        while cont > 0 do
        begin
            fat := fat * cont;
            cont := cont - 1;
        end;
        soma := soma + fat;
        num := Trunc(num/10);
    end;

    if soma = a then
        writeln(a,' eh um numero de Strong')
    else
        writeln(a,' nao eh um numero de Strong');
    
End.

