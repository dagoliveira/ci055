Program perfeito;
Var a, i, soma: Longint;
Begin
    read(a);
    soma := 0;
    i := 1;
    while i < a do
    begin
        if a mod i = 0 then
            soma := soma + i;
        i := i + 1;
    end;
    if soma = a then
        writeln(a,' eh perfeito')
    else
        writeln(a,' nao eh perfeito')
End.
