Program armstrong;
Uses math;
Var a, num, digitos, soma: Longint;
Begin
    read(a);

    (*calcula quantidade de digitos*)
    digitos := Trunc( log10(a) + 1 );

    soma := 0;
    num := a;
    while num > 0 do
    begin
        soma := soma + Trunc(intpower(num mod 10, digitos));
        num := Trunc(num/10);
    end;

    if soma = a then
        writeln(a,' eh um numero de Armstrong')
    else
        writeln(a,' nao eh um numero de Armstrong');
    
End.

