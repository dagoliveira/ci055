program bloco_if;
    var a: longint;
Begin
    read(a);
    if (a > 0) then
    begin
        writeln('Numero maior do que zero');
        writeln('Numero digitado foi: ',a);
    end
    else if (a < 0) then
    begin
        writeln('Numero menor do que zero');
        writeln('Numero digitado foi: ',a);
    end;
End.
