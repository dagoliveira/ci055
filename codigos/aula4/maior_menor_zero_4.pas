program maior_menor_zero_4;
    var a: longint;
Begin
    read(a);
    if (a > 0) then
        writeln('Numero maior do que zero')
    else if (a < 0) then
        writeln('Numero menor do que zero')
    else
        writeln('Numero zero');
End.
