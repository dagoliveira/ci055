program entre_1_e_100000000_while;
var i: Longint;
Begin
    i := 1;
    while i <= 100000000 do
    begin
        writeln(i);
        i := i + 1;
    end;
End.

