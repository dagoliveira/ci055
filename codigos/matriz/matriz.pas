program matrizes;
const max=3;
type matriz=array[1..max,1..max] of longint;

procedure imprime_matriz(var mat: matriz);
(* Imprime na tela uma matriz com max linhas e max colunas *)
var i,j: longint;
begin
	for i:=1 to max do
	begin
		for j:=1 to max do
		begin
			write(mat[i,j]:2, ' ');
		end;
		writeln('');
	end;
end;

procedure soma(var A,B,C : matriz);
(* Soma as Matrizes A e B e salva o resultado em C *)
var i,j: longint;
begin
	for i:=1 to max do
	begin
		for j:=1 to max do
		begin
			C[i,j] := A[i,j] + B[i,j];
		end;
	end;
end;


procedure multiplica(var A,B,C : matriz);
(* Multiplica as Matrizes A e B e salva o resultado em C *)
var i,j,k, soma: longint;
begin
	for i:=1 to max do
	begin
		for j:=1 to max do
		begin
            soma := 0;
		    for k:=1 to max do
		    begin
			    soma := soma + (A[i,k] * B[k,j]);
            end;
            C[i,j] := soma;
		end;
	end;
end;

var
	m1, m2, m_soma, m_mult: matriz;
	i, j, cont: longint;
begin
    (* usar o comando 'TextColor(White);' caso o terminal nao tenha as letras brancas *)
	cont := 1;
	for i:=1 to max do
	begin
		for j:=1 to max do
		begin
			m1[i,j] := cont; (* preenche m1 por linhas *)
			cont := cont + 1;
		end;
	end;

	cont := 1;
	for j:=1 to max do
	begin
		for i:=1 to max do
		begin
			m2[i,j] := cont; (* preenche m2 por colunas *)
			cont := cont + 1;
		end;
	end;

	writeln('m1: ');
	imprime_matriz(m1);

	writeln('m2: ');
	imprime_matriz(m2);


    (* Chama o procedimento 'soma' que salva em m_soma o resultado*)
    soma(m1,m2,m_soma); 

	writeln('');
	writeln('m1 + m2: ');
	imprime_matriz(m_soma);


    (* Chama o procedimento 'multiplica' que salva em m_mult o resultado*)
    multiplica(m1,m2,m_mult);

	writeln('');
	writeln('m1 * m2: ');
	imprime_matriz(m_mult);

	writeln('');
end.
