---
author: Daniel Oliveira
title: Funções e Procedimentos
subtitle: Aula 11 - Alg1 - Turma C
date: Abril/2019
header-includes:
    - \input{babel.tex}
---

# Funções/Procedimentos

- Forma de organizar o código, tendo como vantagem:
    - Modularidade
    - Reaproveitamento de código
    - Legibilidade

- Importante aprender
    - Conceito de variáveis **globais** e **locais**
    - quando usar passagem de parâmetros(argumentos) por **valor** ou por **referência**




# Função

~~~ {.pascal}
function Nome(arg1: Tipo; argN: Tipo): Tipo de retorno;
var
  {declaração de variáveis locais á função 
  quando necessárias}
begin
  {bloco de instruções}
end;
~~~

# Fatorial sem função

~~~ {.pascal}
Program fatorial;
Var i,n, fat: Longint;
Begin
    for i:=1 to 10 do
    begin
        fat := 1;
        n := i;
        while n >= 1 do
        begin
            fat := fat * n;
            n := n - 1;
        end;
        writeln(fat);
    end;
End.
~~~

# Fatorial com função
\small

~~~ {.pascal}
Program fatorial;
Var i: Longint; {Variável global}

function fatorial(n: Longint): Longint;
var fat: Longint; {Variável local, só existe no bloco desta funcao}
Begin
    fat := 1;
    while n >= 1 do
    begin
        fat := fat * n;
        n := n - 1;
    end;
    fatorial := fat; {Retorno da funcao, usa-se o nome dela}
End;

Begin {codigo principal fica simples e legivel}
    for i:=1 to 10 do
        writeln(fatorial(i));
End.
~~~

# Problema: Liste todos os primos até N usando funções
\pause

~~~ {.pascal}
Program primos_funcao;
Const N=100;
Var a: Longint;

function primo(a: Longint): Boolean;
var i: Longint;
Begin
    primo := true; {retorno da funcao inicialmente eh true}
    for i:=2 to Trunc(sqrt(a)) do
        if (a mod i) = 0 then
            primo := false; {altera retorno para false}
End;

Begin
    for a := 2 to N do
        if primo(a) then
            writeln(a);
End.
~~~

# Fatorial com função (**Não funciona**)
\footnotesize

~~~ {.pascal}
Program fatorial;
Var i: Longint; {Variável global}

function fatorial(n: Longint): Longint;
var fat: Longint; {Variável local}
Begin
    fat := 1;
    while n >= 1 do
    begin
        fat := fat * n;
        n := n - 1;
    end;
End; {Acabou sem indicar retorno!}

Begin {codigo (bloco) principal}
    for i:=1 to 10 do
    begin
        fatorial(i);
        writeln(fat);{variável fat nao existe nesse escopo 'bloco'}
    end;
End.
~~~


# Procedimento

- Bloco de código que pode ser reutilizado (não retorna valor nenhum)

~~~ {.pascal}
procedure Nome(arg1: Tipo; argN: Tipo);
var
  {declaração de variáveis locais ao procedimento
  quando necessárias}
begin
  {bloco de instruções}
end;
~~~

# Procedimento, Fatorial (**Usando variável globais**)
\scriptsize

~~~ {.pascal}
Program fatorial;
{Variaveis globais, fat pode ser acessada/alterada 
de qualquer lugar do codigo}
Var i, fat: Longint; 

procedure fatorial(n: Longint);
Begin
    fat := 1;
    while n >= 1 do
    begin
        fat := fat * n;
        n := n - 1;
    end;
End;

Begin {codigo (bloco) principal}
    for i:=1 to 10 do
    begin
        fatorial(i);
        writeln(fat);{variavel fat foi alterada no procedimento fatorial}
    end;
End.
~~~

# Função/Procedimento

- Se é necessário retorno, recomenda-se o uso de função e não de procedimentos
    - O uso de variáveis globais não é recomendado, prejudica a legibilidade e modularidade

# Passagem por valor e referência

- Passagem por valor
    - Usa-se uma cópia (local) da variável
        - Alterar ela no bloco da função/procedimento **não altera** para todo o programa

- Passagem por referência
    - Usa-se a variável verdadeira
        - Alterar ela no bloco da função/procedimento **altera** para todo o programa

# Passagem por valor

- Usa a palavra reservada **var** para indicar uma passagem por referência

~~~ {.pascal}
{'a' por valor e 'b' por referencia}
procedure nome(a:longint; var b:longint); 
begin
    {bloco de codigos}
end;
~~~ 

~~~ {.pascal}
{'a' e 'b' por referencia}
procedure nome(var a,b:longint); 
begin
    {bloco de codigos}
end;
~~~ 

# Trocar dois valores

\small

~~~ {.pascal}
program troca_nao_funciona;
Var a,b: Longint;

procedure troca(a,b: longint);
var temp: longint;
begin
    temp := a;
    a := b;
    b := temp;
end;

Begin
    read(a,b);
    writeln('a: ',a,', b: ',b);
    troca(a,b);
    writeln('a: ',a,', b: ',b);
End.
~~~

\pause
- **Não funciona**

# Trocar dois valores

\small

~~~ {.pascal}
program troca_nao_funciona_2;
Var a,b: Longint;

procedure troca(a: Longint; var b: Longint);
var temp: Longint;
begin
    temp := a;
    a := b;
    b := temp;
end;

Begin
    read(a,b);
    writeln('a: ',a,', b: ',b);
    troca(a,b);
    writeln('a: ',a,', b: ',b);
End.
~~~

\pause
- **Não funciona**

# Trocar dois valores

\small

~~~ {.pascal}
program troca;
Var a,b: Longint;

procedure troca(var a,b: longint);
var temp: longint;
begin
    temp := a;
    a := b;
    b := temp;
end;

Begin
    read(a,b);
    writeln('a: ',a,', b: ',b);
    troca(a,b);
    writeln('a: ',a,', b: ',b);
End.
~~~

\pause
- **Funciona**

# Trocar dois valores usando vetor

\columnsbegin
\column{.65\textwidth}

\scriptsize

~~~ {.pascal}
program troca;
Var a: Array[1..10] of Longint;
    i: longint;

procedure troca(var a,b: longint);
var temp: longint;
begin
    temp := a;
    a := b;
    b := temp;
end;

Begin
    for i:= 1 to 10 do {Inicia o vetor}
        a[i] := i;
    for i:= 1 to 10 do {Imprime o vetor}
        write(a[i]:3);

    troca(a[2],a[8]); {troca duas posicoes}
    writeln();

    for i:= 1 to 10 do {Imprime o vetor}
        write(a[i]:3);
End.
~~~

\column{.35\textwidth}

\pause

- Saída:

  1  **2**  3  4  5  6  7  **8**  9 10

  1  **8**  3  4  5  6  7  **2**  9 10

\columnsend

# Passando vetores como parâmetros

- **Não aceito** em Pascal:

~~~ {.pascal}
    function nome (A: array[1..10] of Longint): Longint;
~~~

\pause
\vfill

- Necessário **definir um tipo** novo:

~~~ {.pascal}
    type tipo_vetor=Array[1..1000] of Longint;
    function nome (A: tipo_vetor): Longint;
~~~

# Procedimento para trocar valores de um vetor 

\scriptsize

~~~ {.pascal}
program troca;
type tipo_vetor=Array[1..10] of Longint; {tipo_vetor define um vetor de tamanho 10}
Var i: longint;
    a : tipo_vetor; {vetor de tamanho 10}

{lembrem de passar o vetor como referência, usando 'var'}
procedure troca(var v: tipo_vetor; a,b: longint); 
var temp: longint;
begin
    temp := v[a];
    v[a] := v[b];
    v[b] := temp;
end;

Begin
    for i:= 1 to 10 do {Inicia o vetor}
        a[i] := i;
    for i:= 1 to 10 do {Imprime o vetor}
        write(a[i]:3);

    troca(a,8,2); {troca duas posicoes, informa o nome do vetor e os indices}
    writeln();
    for i:= 1 to 10 do {Imprime o vetor}
        write(a[i]:3);
End.
~~~

