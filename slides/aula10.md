---
author: Daniel Oliveira
title: Vetores II
subtitle: Aula 10 - Alg1 - Turma C
date: Abril/2019
header-includes:
    - \input{babel.tex}
---

# Vetores

- Usar o apenas um nome de variável e acessar diversas posições por índices únicos

    - Exemplo: separar 10 posições de memória para variáveis do tipo Longint

~~~ {.pascal}
        var Vetor: array[1..10] of Longint;
~~~


| índice   | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 |
|----------|---|---|---|---|---|---|---|---|---|----|
| conteúdo | ? | ? | ? | ? | ? | ? | ? | ? | ? | ?  |

# Problema básico

- Usando vetores leia 10 números reais e informe o índice do maior e o índice do menor valor
\pause

\scriptsize

~~~ {.pascal}
Program maior_menor;
Const MAX=10;
Var vetor: array[1..MAX] of real;
i, maior, menor: longint;
Begin
    for i:= 1 to MAX do
    begin
        read(vetor[i]);
    end;

    maior := 1;
    menor := 1;
    for i:= 2 to MAX do
    begin
        if vetor[i] > vetor[maior] then
            maior := i;
        if vetor[i] < vetor[menor] then
            menor := i
    end;

    writeln('maior: ',maior,', menor: ',menor);
End.
~~~

# Primos novamente

- Como gerar uma lista de números primos até um limite máximo N?

\pause
\scriptsize

~~~ {.pascal}
Program primos_sem_vetor;
Const N=5000;
Var a,b: Longint;
    primo: Boolean;
Begin
    a := 2;
    while a <= N do
    begin
        primo := true;
        b := 2;
        while b <= sqrt(a) do
        begin
            if (a mod b) = 0 then
                primo := false;
            b := b + 1;
        end;

        if primo then
            writeln(a);
        a := a + 1;
    end;
End.
~~~

# Primos novamente

- Como gerar uma lista de números primos até um limite máximo N **usando vetores**?
\pause

- Crivo de Eratóstenes (algoritmo criado por volta de 200 a.C.)
    1. Crie uma lista de números consecutivos de 2 até N
    2. Inicie p com 2, menor número primo
    3. Marque os números múltiplos de p, incrementando na lista de 2*p até N (2*p, 3*p, 4*p, ...)
    4. p deve receber o próximo número na lista não marcado, se não tiver mais números não marcados pare, volte ao passo 3 caso contrário.
    5. Todos os números não marcados forma a lista de números primos até N.

# Exemplo do Crivo de Eratóstenes

- Lista de 2 até 15:

2 3 4 5 6 7 8 9 10 11 12 13 14 15 
\pause

- P = 2
\pause

2 3 **4** 5 **6** 7 **8** 9 **10** 11 **12** 13 **14** 15

\pause
- Próximo número não marcado, P = 3
\pause

2 3 **4** 5 **6** 7 **8** **9** **10** 11 **12** 13 **14** **15**

\pause

...


- P = 15, acabou a lista ficou:

2 3 **4** 5 **6** 7 **8** **9** **10** 11 **12** 13 **14** **15**

# Primos novamente

- Como gerar uma lista de números primos até um limite máximo N **usando vetores**?
\pause

\tiny

~~~ {.pascal}
Program primos_vetor;
Const N=1000000;
Var i,j: Longint;
vetor :array[2..N] of boolean;
Begin
    for i := 2 to N do
    begin
        vetor[i] := true;
    end;
    for i := 2 to N do
    begin
        if vetor[i] then
        begin
            j:= i + i;
            while j <= N do
            begin
                vetor[j] := false;
                j := j + i;
            end;
        end;
    end;
    for i := 2 to N do
    begin
        if vetor[i] then
            writeln(i)
    end;
End.
~~~

