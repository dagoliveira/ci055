---
author: Daniel Oliveira
title: Vetores com Funções e Procedimentos I
subtitle: Aula 12 - Alg1 - Turma C
date: Abril/2019
header-includes:
    - \input{babel.tex}
---

# Índice do maior elemento

- Considerando a seguinte definição de vetor:

~~~ {.pascal}
type vetor=array[1..MAX] of real;
~~~

1. Faça uma função para retornar o índice do **maior** elemento do vetor
2. Faça uma função para retornar o índice do **menor** elemento do vetor
3. Faça uma função para retornar a média de todos os elementos do vetor

# Maior elemento

~~~ {.pascal}
type vetor=array[1..MAX] of real;

function maior(var V:vetor; tamanho: longint): Longint;
var i, m: Longint;
Begin
    m := 1;
    for i:=2 to tamanho do
        if V[i] > V[m] then
            m := i;

    maior := m;
End;
~~~

# Menor elemento

~~~ {.pascal}
type vetor=array[1..MAX] of real;

function menor(var V:vetor; tamanho: longint): Longint;
var i, m: Longint;
Begin
    m := 1;
    for i:=2 to tamanho do
        if V[i] < V[m] then
            m := i;

    menor := m;
End;
~~~

# Média

~~~ {.pascal}
type vetor=array[1..MAX] of real;

function media(var V:vetor; tamanho: longint): Real;
var i: Longint; soma : Real;
Begin
    soma := 0;
    for i:=1 to tamanho do
        soma := soma + V[i];

    media := soma/tamanho;
End;
~~~

# Usando as funções

- Faça um programa principal que leia o vetor do teclado até que o usuário entre com um zero. Após use as funções anteriores

\pause

~~~ {.pascal}
Begin
    i := 0;
    writeln('Entre com no maximo ', MAX,' numeros, digite 0 para terminar:');
    repeat
        i := i + 1;
        read(A[i]);
    until A[i] = 0;
    maior_indice := maior(A, i-1);
    menor_indice := menor(A, i-1);
    media_vetor := media(A, i-1);
    writeln('indice do maior elemento: ',maior_indice);
    writeln('indice do menor elemento: ',menor_indice);
    writeln('media do vetor: ',media_vetor:3:2);
End.
~~~


# Busca em vetores

- Uma pergunta muito frequente é:
    - Certo elemento está ou não no vetor? 

\vfill

- Busca por elemento é essencial em computação
    - Busca livros, CPF, dados num arquivo, ...

\vfill
\pause

- Como buscar elementos no vetor? Faça um algoritmo para isso


# Busca genérica

\small

~~~ {.pascal}
(*retorna zero caso nao encontre o elemento, ou a posicao 
do elemento caso ele esteja no vetor*)
function busca(var V:vetor; elemento: real; 
                    tamanho: longint): Longint;
var i, cont: Longint;
Begin
    cont := 0;
    i:=1;
    while ( V[i] <> elemento) and (i <= tamanho) do
    begin
        cont := cont + 1;
        i := i + 1;
    end;
    if i <= tamanho then
        busca := i
    else
        busca := 0;
End;
~~~

# Busca genérica

- Quanto tempo leva para encontrar um elemento num vetor de tamanho N? \pause
    - Pior caso: **N**
    - Caso médio: **N/2**
    - Melhor caso: **1**

\pause
\vfill

- E se o vetor está ordenado?


# Busca binária

- Eliminar metade do vetor a cada passo
    - se o elemento no meio é **menor**, usar metade da **direita**
    - se o elemento no meio é **maior**, usar a metade da **esquerda**

\vfill
\pause

- Exemplo: Dado o seguinte vetor, buscar pelo elemento 15

~~~
1 3 4 5 8 9 15 25 64 78
~~~

# Busca binária, Buscando pelo 15

![](./img/busca_binaria.png)

\pause
- Parar se o elemento do meio for o elemento buscado



- Se chegou ao fim, apenas um elemento restante, verificar se o elemento é o buscado


# Busca binária, tempo no pior caso

\centering
![](./img/busca_binaria_log2.png){width=100%}

\pause

- Quantas vezes precisamos elevar 2 para que $2^{i}$ seja igual a $n$?

# Tempo de execução

\columnsbegin
\column{.6\textwidth}

- Pior caso para um vetor de tamanho N:
    - Busca sequencial: N
    - Busca binária: $Log_{2}N$

\column{.4\textwidth}
\pause

|   N      |  Log2 N |
|----------|---------|
|    2     |    1    |
|    4     |    2    |
|    8     |    3    |
|    16    |    4    |
|    32    |    5    |
|    ...   |   ...   |
| 1 milhão |   ~20   |
|    ...   |   ...   |
| 1 bilhão |   ~30   |

\columnsend

# Como implementar busca binária em pascal?

\pause

\footnotesize

~~~ {.pascal}
(*retorna 0 caso nao encontre o elemento, 
ou a posicao do elemento caso ele esteja no vetor*)
function busca_binaria(var V:vetor; elemento: real; 
                             tamanho: longint): Longint;
var inicio, fim, meio : Longint;
Begin
    inicio:=1;
    fim:= tamanho;
    meio:= (inicio + fim) div 2;
    while (v[meio] <> elemento) and (fim >= inicio) do
    begin
        if v[meio] > elemento then
            fim:= meio - 1
        else
            inicio:= meio + 1;
        meio:= (inicio + fim) div 2;
     end;
     if v[meio] = elemento then
        busca_binaria:= meio
     else
        busca_binaria:= 0;
End;
~~~
