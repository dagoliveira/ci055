---
author: Daniel Oliveira
title: Comandos de desvio e repetição II
subtitle: Aula 5 - Alg1 - Turma C
date: Março/2019
header-includes:
    - \input{babel.tex}
---

# Comandos de desvio condicional
- Comandos apenas no caminho **verdadeiro**

~~~ {.pascal}
    if (<expressão boleana>) then
    begin
        <comandos caminho verdadeiro>
    end
~~~

- Comandos no caminho **verdadeiro** e **falso**

~~~ {.pascal}
    if (<expressão boleana>) then
        begin
            <comandos caminho verdadeiro>
        end
    else
        begin
            <comandos caminho falso>
        end
~~~

# Comandos de desvio condicional encadeado

- Comandos no caminho **verdadeiro** e **falso**

~~~ {.pascal}
    if (<expressão boleana>) then
        begin
            <comandos>
        end
    else if (<outra expressão>) then
        begin
            <comandos>
        end
    else if (<outra expressão>) then
        begin
            <comandos>
        end
    else
        begin
            <comandos>
        end
~~~

# Comando while

- Laço do while será repetido enquanto a condição for verdadeira

~~~ {.pascal}
    while (<expressão boleana>) do
    begin
        <comandos>
    end;
~~~

# Passos fundamentais de comandos de repetição

1. Inicialização da variável de controle
2. Expressão boleana com a variável de controle
3. Alteração do valor da variável de controle

~~~ {.pascal}
program entre_1_e_100000000_while;
var i: Longint;
Begin
    i := 1; (*Inicialização*)
    while (i < 10) do (* Expressão*)
    begin
        <comandos>
        i := i + 1; (*Exemplo de alteração*)
    end;
End.
~~~

# Variáveis Contadoras
\scriptsize

~~~ {.pascal}
Program qualquer;
Var cont: Longint;
Begin
    cont := 0 (*Inicialização*)
    while (<expressão boleana>) do
    begin
        <comandos>
        cont := cont + 1; (*Incremento*)
    end;
End.
~~~

~~~ {.pascal}
Program qualquer_outro;
Var cont: Longint;
Begin
    cont := 0 (*Inicialização*)
    while (<expressão boleana>) do
    begin
        <comandos>
        if <expressão boleana> then
            cont := cont + 1; (*Incremento sempre que o evento acontecer*)
    end;
End.
~~~
\normalsize

# Variáveis Acumuladoras


~~~ {.pascal}
Program qualquer;
Var acumula, a: Longint;
Begin
    cont := 0 (*Inicialização*)
    while (<expressão boleana>) do
    begin
        read(a)
        <comandos>
        acumula := acumula + a; (*Incremento*)
    end;
End.
~~~

# Variáveis Sinalizadoras

\small

~~~ {.pascal}
Program qualquer;
Var sinalizador: Boolean;
Begin
    sinalizador := false (*Inicialização*)
    while (<expressão boleana>) do
    begin
        <comandos>
        if <expressao boleana> then (*Testa se algo aconteceu*)
            sinalizador := true; (*sinaliza que algo aconteceu*)
    end;

    if sinalizador then
        writeln('evento aconteceu')
    else
        writeln('evento nao aconteceu');
End.
~~~

# Problemas Básicos

- Leia um número (média final de um aluno e informe o seguinte:
    - se for maior ou igual a 9.0, informe 'Aprovado, muito bem'
    - se for maior ou igual a 8.0, informe 'Aprovado'
    - se for maior ou igual a 7.0, informe 'Aprovado, por pouco'
    - se for menor do que 7.0, informe 'Reprovado'
\pause

\small

~~~ {.pascal}
Program media_aluno;
Var media: Real;
Begin
    read(media);
    if media >= 9.0 then
        writeln('Aprovado, muito bem')
    else if media >= 8.0 then
        writeln('Aprovado')
    else if media >= 7.0 then
        writeln('Aprovado, por pouco')
    else
        writeln('Reprovado');
End.
~~~
\normalsize

# Problemas Básicos

- Dado três números, informar o maior deles.

\pause

~~~~ {.pascal}
Program maior_de_tres;
Var a,b,c: Longint;
Begin
    read(a,b,c);
    if a > b then
        if a > c then
            writeln(a)
        else
            writeln(c)
    else
        if b > c then
            writeln(b)
        else
            writeln(c);
End.
~~~~

# Problemas Básicos

- Leia várias trincas de números e informe o maior
    - pare de ler trincas quando os três números forem iguais a zero (0,0,0)

\pause
\scriptsize

~~~ {.pascal}
Program trincas;
Var a,b,c: Longint;
Begin
    read(a,b,c);
    while (a <> 0) or (b <> 0) or (c <> 0) do 
    begin
        if a > b then
            if a > c then
                writeln(a)
            else
                writeln(c)
        else
            if b > c then
                writeln(b)
            else
                writeln(c);
        read(a,b,c);
   end
End.
~~~~

\normalsize


# Problemas Básicos

- Leia 10 números e imprima o maior

\pause

~~~ {.pascal}
Program maior_de_10;
Var a,maior,cont: Longint;
Begin
    read(a);
    maior := a;
    cont := 1;
    while cont < 10 do 
    begin
        read(a);
        if a > maior then
            maior := a;
        cont := cont + 1;
    end;
    writeln('maior: ',maior);
End.
~~~

# Problemas Básicos

- Leia 10 números e imprima o maior e em qual posição ele está

\pause
\scriptsize

~~~ {.pascal}
Program maior_de_10_com_pos;
Var a,maior,cont,posicao: Longint;
Begin
    read(a);
    maior := a;
    cont := 1;
    posicao := 1;
    while cont < 10 do 
    begin
        read(a);
        if a > maior then
        begin
            maior := a;
            posicao := cont + 1;
        end;
        cont := cont + 1;
    end;
    writeln('maior: ',maior,' pos: ',posicao);
End.
~~~
\normalsize

# Problemas Básicos

- Ler um número maior do que 1 e informar se ele é primo ou não
\pause

\tiny

~~~ {.pascal}
Program testa_primo;
Var 
    a,b: Longint;
    primo: Boolean;
Begin
    read(a);
    if a <= 1 then (* Se o numero for menor ou igual a 1, nao faz nada *)
        writeln('Entre com um numero maior do que 1')
    else (*Se o numero for maior do que 1, testa se eh primo *)
    begin
        primo := true;
        b := 2;

        while b <= sqrt(a) do
        begin
            if (a mod b) = 0 then
                primo := false;
            b := b + 1;
        end;

        if primo then
            writeln('Eh primo')
        else
            writeln('Nao eh primo');
    end;
End.
~~~
\normalsize

# Problemas Básicos

- Imprimir todos os números primos entre 2 e 500
\pause

\scriptsize

~~~ {.pascal}
Program primos_2_500;
Var 
    a,b: Longint;
    primo: Boolean;
Begin
    a := 2;
    while a <= 500 do
    begin
        primo := true;
        b := 2;
        while b <= sqrt(a) do
        begin
            if (a mod b) = 0 then
                primo := false;
            b := b + 1;
        end;

        if primo then
            writeln(a);
        a := a + 1;
    end;
End.
~~~

\normalsize

