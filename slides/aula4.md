---
author: Daniel Oliveira
title: Comandos de desvio e repetição
subtitle: Aula 4 - Alg1 - Turma C
date: Março/2019
header-includes:
    - \input{babel.tex}
---

# Lógica Booleana ~1847


- Álgebra Booleana
    - Variáveis assumem apenas **dois** valores: **V**, **F**
    - Operadores: **e(AND)**, **ou(OR)**, **não(NOT)**

\pause
- Operadores relacionais
    - '='   igual
    - '<>'  diferente
    - '>'   maior
    - '>='  maior ou igual
    - '<'   menor
    - '<='  menor ou igual

\pause
- Exemplos de expressões booleanas
    - (media >= 7.0) and (faltas <= 0.75)
    - not (velocidade >= 80)

# Problema: Informar se o número é positivo



~~~ {.pascal}
program maior_zero;
    var a: longint;
Begin
    read(a);
    if (a > 0) then
        writeln('Numero maior do que zero');
End.
~~~


# Problema: Informar se o número é positivo ou negativo


~~~ {.pascal}
program maior_menor_zero;
    var a: longint;
Begin
    read(a);
    if (a > 0) then
        writeln('Numero maior do que zero');
    if (a < 0) then
        writeln('Numero menor do que zero');
End.
~~~


# Problema: Informar se o número é positivo ou negativo melhorado


~~~ {.pascal}
program maior_menor_zero_2;
    var a: longint;
Begin
    read(a);
    if (a > 0) then
        writeln('Numero maior do que zero')
    else
        writeln('Numero menor do que zero');
End.
~~~
\pause
- O algoritmo funciona?
\pause
- Notem que no comando antes do **else** não vai o '**;**'

# Problema: Informar se o número é positivo ou negativo melhorado versão correta


~~~ {.pascal}
program maior_menor_zero_3;
    var a: longint;
Begin
    read(a);
    if (a > 0) then
        writeln('Numero maior do que zero')
    else if (a < 0) then
        writeln('Numero menor do que zero');
End.
~~~


# Problema: Informar se o número é positivo, negativo ou zero


~~~ {.pascal}
program maior_menor_zero_4;
    var a: longint;
Begin
    read(a);
    if (a > 0) then
        writeln('Numero maior do que zero')
    else if (a < 0) then
        writeln('Numero menor do que zero')
    else
        writeln('Numero zero');
End.
~~~

# Mais de um comando dentro do desvio


~~~ {.pascal}
program bloco_if;
    var a: longint;
Begin
    read(a);
    if (a > 0) then
    begin
        writeln('Numero maior do que zero');
        writeln('Numero digitado foi: ',a);
    end
    else if (a < 0) then
    begin
        writeln('Numero menor do que zero');
        writeln('Numero digitado foi: ',a);
    end;
End.
~~~

# Comandos de desvio condicional
- Comandos apenas no caminho **verdadeiro**
~~~ {.pascal}
    if (<expressão boleana>) then
    begin
        <comandos caminho verdadeiro>
    end
~~~

- Comandos no caminho **verdadeiro** e **falso**
~~~ {.pascal}
    if (<expressão boleana>) then
        begin
            <comandos caminho verdadeiro>
        end
    else
        begin
            <comandos caminho falso>
        end
~~~

# Comandos de desvio condicional encadeado

- Comandos no caminho **verdadeiro** e **falso**
~~~ {.pascal}
    if (<expressão boleana>) then
        begin
            <comandos>
        end
    else if (<outra expressão>) then
        begin
            <comandos>
        end
    else if (<outra expressão>) then
        begin
            <comandos>
        end
    else
        begin
            <comandos>
        end
~~~

# Problema: Imprimir todos os números entre 1 e 10


~~~ {.pascal}
program entre_1_e_10;
Begin
    writeln(1);
    writeln(2);
    writeln(3);
    writeln(4);
    writeln(5);
    writeln(6);
    writeln(7);
    writeln(8);
    writeln(9);
    writeln(10);
End.
~~~


# Problema: Imprimir todos os números entre 1 e 100000000

- Muita paciência seria necessária e muitos bugs (copy and paste) aconteceriam

~~~ {.pascal}
program entre_1_e_100000000;
Begin
    writeln(1);
    writeln(2);
    writeln(3);
    writeln(4);
    writeln(5);
    writeln(6);
    writeln(7);
    writeln(8);
    writeln(9);
    writeln(10);
    ...
End.
~~~


# Problema: Imprimir todos os números entre 1 e 100000000

- Agora é possível imprimir todos os números até 100000000?

~~~ {.pascal}
program entre_1_e_100000000;
var i: Longint;
Begin
    i := 1;

    writeln(i);
    i := i + 1;

    writeln(i);
    i := i + 1;

    writeln(i);
    i := i + 1;
    ...
End.
~~~

# Solução: Imprimir todos os números entre 1 e 100000000

~~~ {.pascal}
program entre_1_e_100000000_while;
var i: Longint;
Begin
    i := 1;
    while i <= 100000000 do
    begin
        writeln(i);
        i := i + 1;
    end;
End.
~~~

# Comando while

- Laço do while será repetido enquanto a condição for verdadeira

~~~ {.pascal}
    while (<expressão boleana>) do
    begin
        <comandos>
    end;
~~~

# Passos fundamentais de comandos de repetição

1. Inicialização da variável de controle
2. Expressão boleana com a variável de controle
3. Alteração do valor da variável de controle

~~~ {.pascal}
program entre_1_e_100000000_while;
var i: Longint;
Begin
    i := 1;
    while (i < 10) do
    begin
        <comandos>
        i := i + 1; (*Exemplo de alteração*)
    end;
End.
~~~

# Quantas vezes os comandos do laço serão executados?

- 10 vezes? 9 vezes? 8 vezes?

~~~ {.pascal}
program entre_1_e_100000000_while;
var i: Longint;
Begin
    i := 1;
    while (i < 10) do
    begin
        <comandos>
        i := i - 1;
    end;
End.
~~~

# Quantas vezes os comandos do laço serão executados?

- E agora?

~~~ {.pascal}
program entre_1_e_100000000_while;
var i: Longint;
Begin
    i := 1;
    while (i < 10) do
    begin
        <comandos>
    end;
End.
~~~

# Quantas vezes os comandos do laço serão executados?

- E agora vai dar certo?

~~~ {.pascal}
program entre_1_e_100000000_while;
var i: Longint;
Begin
    while (i < 10) do
    begin
        <comandos>
        i := i + 2;
    end;
End.
~~~

# Problema: Ler números até que o usuário entre com o número zero

- Não sabemos a priori quantos números serão digitados
    - Programa deve ser dinâmico

\pause

~~~ {.pascal}
program le_numeros;
var i: Longint;
Begin
    read(i);
    while i <> 0 do
    begin
        read(i);
    end;
End.
~~~

# Problemas com entrada dinâmica de dados

- Usando o algoritmo anterior como base resolva os seguintes problemas:
    - Contar quantos números foram entrados, excluindo o zero
    - Somar todos os números entrados
    - Informar para cada número se ele é par ou ímpar (**mod** retorna o resto da divisão inteira, ex: "10 mod 4" retorna 2 )

~~~ {.pascal}
program le_numeros;
var i: Longint;
Begin
    read(i);
    while i <> 0 do
    begin
        read(i);
    end;
End.
~~~
