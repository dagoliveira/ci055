---
author: Daniel Oliveira
title: Fluxograma
subtitle: Aula 2 - Alg1 - Turma C
date: Fevereiro/2019
header-includes:
    - \input{babel.tex}
---

# Professor

- Daniel Alfonso Gonçalves de Oliveira
    - dagoliveira@inf.ufpr.br
    - Sala 60 - Departamento de Informática

# Departamento de Informática

- Dicas sobre o departamento: http://www.inf.ufpr.br/ 
    - "Infraestrutura"
    - "Infraestrutura Tecnológica"
    - "Dúvidas frequentes sobre suporte aos sistemas"
\vspace{1em}

- Logins e senhas para os laboratórios já estão na secretaria
    - Pegar na recepção e alterar imediatamente (comando **passwd**)

# Disciplina

- Página da disciplina: http://www.inf.ufpr.br/cursos/ci055/
\vspace{1em}

- Ferramenta para aprendizado:
    - FARMA-ALG: http://ufpr.farma-alg.com.br/
    - Correção automática de exercícios
\vspace{1em}

# Sistema Operacional

- **Linux será necessário** até o final do curso (dual boot, máquina virtual)

    ![](img/tux.png){width=10%}

- Linux Mint usado nos laboratórios

    ![](img/linux-mint.png){width=30%}

- Podem usar qualquer distribuição (Ubuntu, Fedora, Debian, OpenSUSE, ...)

# Avaliação

- 3 Provas escritas
    - Média = (P1 + 2*P2 + 3*P3)/6
\vspace{1em}

- Aprovação: 75\% de presença e nota >= 7,0
\vspace{1em}

- Prova Final: 75\% de presença e nota >= 4,0 e nota < 7,0
    - Nota final = (Média + Prova Final)/2
\vspace{1em}

- Plano de aulas (datas das provas), sistema de avaliação e provas antigas na página principal da disciplina
    - http://www.inf.ufpr.br/cursos/ci055/

# Introdução aos algoritmos

- Objetivos:
\vspace{1em}
    - Apresentar o conceito de algoritmos
\vspace{1em}
    - Apresentar o conceito de programas
\vspace{1em}
    - Entender o nível de detalhamento máximo para se programar um computador

# Dos problemas às soluções

- A computação existe porque queremos resolver um problema usando um computador 
- Vimos que, dado um problema, existem diversas alternativas de solução

- Algumas soluções são melhores do que outras sob determinadas condições

- Uma vez escolhida a técnica de solução de um problema deve-se escrevê-la em forma algoritmica

# O que é um algoritmo?

- É uma sequência suficientemente precisa de instruções que detalha o processo de solução de um problema e que, quando executada por outra pessoa ou por uma máquina a partir dos dados de entrada, leva à solução do problema

# Para que serve um algoritmo?

1- Serve para registrar o processo de solução de um problema

\vspace{1em}
2- Serve para que outra pessoa possa resolver o mesmo problema, sem ter que reinventar a roda

# Exemplo: receita de bolo

1- Bata em uma batedeira a manteiga e o açucar

2- Junte as gemas uma a uma até obter um creme homogêneo

3- Adicione o leite aos poucos 

4- Desligue a batedeira e adicione a farinha de trigo, o chocolate em pó, o fermento e reserve

5- Bata as claras em neve e junte-as à massa de chocolate misturando delicadamente

6- Unte uma forma retangular pequena com manteiga e farinha 

7- leve para assar em forno médio pré-aquecido por aproximadamente 30 minutos

8- Desenforme o bolo ainda quente e reserve


# Características da receita de bolo

- Outra pessoa pode fazer o bolo
\vspace{1em}
- As frases estão no imperativo
\vspace{1em}
- Existe uma ordem para executar as ações
\vspace{1em}
- Algumas ações devem esperar outras terminarem
\vspace{1em}
- Algumas ações podem ter a ordem invertida, outras não


# Exemplo: receita de bolo

1- Bata em uma batedeira a manteiga e o açucar

2- Junte as gemas uma a uma até obter um creme homogêneo

3- Adicione o leite aos poucos 

4- Desligue a batedeira e adicione a farinha de trigo, o chocolate em pó, o fermento e reserve

**7- leve para assar em forno médio pré-aquecido por aproximadamente 30 minutos**

5- Bata as claras em neve e junte-as à massa de chocolate misturando delicadamente

6- Unte uma forma retangular pequena com manteiga e farinha 

8- Desenforme o bolo ainda quente e reserve

# Exemplo: receita de bolo

1- Bata em uma batedeira a manteiga e o açucar

**6- Unte uma forma retangular pequena com manteiga e farinha**

2- Junte as gemas uma a uma até obter um creme homogêneo

3- Adicione o leite aos poucos 

4- Desligue a batedeira e adicione a farinha de trigo, o chocolate em pó, o fermento e reserve

5- Bata as claras em neve e junte-as à massa de chocolate misturando delicadamente

7- leve para assar em forno médio pré-aquecido por aproximadamente 30 minutos

8- Desenforme o bolo ainda quente e reserve

# Problemas da receita de bolo

- Algumas ações são omitidas, quem as escreveu acha que são óbvias (ex.: separar as gemas das claras)
\vspace{1em}

- Quem escreveu a receita espera que a outra que vai fazer o bolo tenha um mínimo de conhecimento culinário
\vspace{1em}

- Algumas instruções são obscuras
    - até obter um creme homogêneo
    - misturando delicadamente
    - forno médio pré-aquecido

# Solução: detalhar melhor a solução

- Pedir para quem escreveu a receita explicar melhor...
- Como se faz claras em neve?
- Como se separa a gema do ovo?
- Como se quebra um ovo?
- Onde achar um ovo?
- Uso ovo marrom ou branco?
- Pode ser ovo de codorna?
\vfill
- Quando se pode parar de detalhar ???

# Nível máximo de detalhamento
- É preciso se convencionar o nível máximo de detalhamento
\vspace{1em}

- Assim, pode-se escrever a receita de bolo neste nível
\vspace{1em}

- Por exemplo: considera-se que quem vai fazer o bolo já fez estágio em alguma doceria por pelo menos 2 anos
\vspace{1em}

- Agora o algoritmo (receita) pode permanecer do jeito que está

# O que é um programa?

- Um programa é um algoritmo escrito com o nível de detalhamento pré-definido para a máquina ou pessoa que vai executá-lo
\vspace{1em}

- Ele deve ser escrito de maneira extremamente precisa e rigorosa, sem ambiguidades, e deve produzir o mesmo resultado a cada execução
\vspace{1em}

- A diferença para um algoritmo é que este pode ser escrito em mais alto nível e em linguagem menos formal.
\vspace{1em}

# Como gerar um programa?

![](img/aula1-ling-natural-maquina.png)


# Qual nível máximo de detalhamento nos computadores?

- Os computadores modernos usam o nível máximo de detalhamento conforme definido por John von Neumann em 1946


# Fluxograma - formalizando algoritmos

![](img/aula1-fluxograma.png)

# Fulxograma Exemplo - Trocando pneu de um carro
\centerbegin
![](img/aula1-flux-trocar-pneu.png)
\centerend

# Fulxograma Exemplo - Trocando pneu de um carro 2

\centerbegin
![](img/aula1-flux-peneu-2.png)
\centerend

# Exercício

- Faça o fluxograma de um algoritmo para resolver equções de $2^{\circ}$ grau

\centerbegin
    $ax^2 + bx + c = 0$
\centerend

\vspace{1em}

- Soluções:

    - $X_1 = \frac{-b + \sqrt{b^2 -4ac} }{2a}$
    \vspace{1em}
    - $X_2 = \frac{-b - \sqrt{b^2 -4ac} }{2a}$

# Fulxograma Equação Segundo Grau

\centerbegin
![](img/aula1-flux-seg-grau.png){width=100%}
\centerend

# Exercício 2

- Faça o fluxograma de um algoritmo que leia **3 números** e informe o **maior** deles
