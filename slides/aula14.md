---
author: Daniel Oliveira
title: Exercícios Vetores
subtitle: Aula 14 - Alg1 - Turma C
date: Abril/2019
colorlinks: true
header-includes:
    - \definecolor{Blue}{HTML}{0000FF}
    - \input{babel.tex}
---

# Problema 1

- Contar quantos vezes cada dígito aparece numa sequência de números

- Exemplo: 
    - Sequência 15 19 56 98 25 3287
    - Resposta:

    | Dígito | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
    |--------|---|---|---|---|---|---|---|---|---|---|
    |   #    | 0 | 2 | 2 | 1 | 0 | 3 | 1 | 1 | 2 | 2 |

# Problema 2

- Dada uma sequencia de números inteiros, determinar os números que compõem a sequência e o número de vezes que cada um deles ocorre

- Exemplo:
    - Sequência: 4 0 4 8 6 7 1 7 2 6 1 6 6 7 9 5 5 6 9 9 
    - Resposta:

    | Número | 0 | 4 | 8 | 6 | 7 | 1 | 2 | 9 | 5 |
    |--------|---|---|---|---|---|---|---|---|---|
    |   #    | 1 | 2 | 1 | 5 | 3 | 2 | 1 | 3 | 2 |

# Problema 3

- Dado dois vetores, imprimir apenas os elementos que aparecem nos dois vetores, sem imprimir o mesmo elemento mais de uma vez

- Exemplo:
    - Vetor 1: 1 5 6 4 8 2 3 5 4 1 5
    - Vetor 2: 5 8 6 3 2 7 2 5 3
    - Resposta: 5 6 8 2 3 
