---
author: Daniel Oliveira
title: Ordenação
subtitle: Aula 13 - Alg1 - Turma C
date: Abril/2019
colorlinks: true
header-includes:
    - \definecolor{Blue}{HTML}{0000FF}
    - \input{babel.tex}
---

# Trocar dois valores

- Usando passagem pro referência a seguinte procedure troca quaisquer valores (em vetores ou de variáveis simples)

~~~ {.pascal}
procedure troca(var a,b: longint);
var temp: longint;
Begin
    temp := a;
    a := b;
    b := temp;
End;
~~~

# Gerando números pseudoaleatórios 

~~~ {.pascal}
const MAX=10;
type vetor=array [1..MAX] of longint;
var
   a: vetor;
   i: integer;
begin
    randomize; (*Uma vez somente no programa*)
    for i := 1 to MAX do
    begin
        (*Gera numero aleatorio entre 0 e 99*)
         a[i] := Random(99); 
    end;
end.
~~~

# Ordenação pelo método bolha

- Ideia:
    1. Percorra todo o vetor da primeira posição até a penúltima, comparando elementos em pares (dois a dois)
    2. Se o elemento de menor índice for menor, trocar elementos
    3. Repita o processo N vezes (N -> tamanho do vetor)

# Ordenação pelo método bolha

\centering
![](./img/bubble-sort.png)

\href{https://gitlab.com/dagoliveira/ci055/blob/master/slides/ordenacao-animacoes/Bubble-sort.gif}{Animação link}

# Ordenação pelo método bolha

~~~ {.pascal}
procedure bubble_sort(var v: vetor; tam: longint);
var
   i, j: longint;
begin
     for i := 1 to (tam-1) do
     begin
          for j := 1 to (tam-1) do
          begin
               if v[j] > v[j+1] then
               begin
                    troca(v[j], v[j+1]);
               end;
          end;
     end;
end;
~~~

# Ordenação por seleção

- Ideia:
    1. Selecionar o menor elemento do vetor
    2. Colocar o menor elemento no início do vetor
    3. Repetir o processo, mas considerando o vetor iniciando na posição subsequente ao menor elemento atual


# Ordenação por seleção

\centering

![](./img/selection-sort.jpg){height=60%}\

\href{https://gitlab.com/dagoliveira/ci055/blob/master/slides/ordenacao-animacoes/Selection-Sort.gif}{Animação link}

# Ordenação por seleção

~~~ {.pascal}
procedure selection_sort(var v: vetor; tam: longint);
var
   i, j, imenor: longint;
begin
    for i:=1 to tam do
    Begin
        imenor:=i;
        for j:=i+1 to tam do
        Begin
            if v[j] < v[imenor] then
                imenor:=j;
        End;
        troca(v[i],v[imenor]);
    End;
end;
~~~

# Ordenação por inserção

- Ideia:
    1. Iniciar com o primeiro elemento do vetor como uma lista ordenada de um elemento
    2. Selecionar o próximo elemento do vetor, inserir na lista na posição correta aumentando o tamanho da lista 
    3. Repetir o passo 2 até que não tenha mais elementos que não foram inseridos


# Ordenação por inserção

\centering

![](./img/insertion_sort.png){height=60%}\

\href{https://gitlab.com/dagoliveira/ci055/blob/master/slides/ordenacao-animacoes/Insertion-sort-example.gif}{Animação link}

# Ordenação por inserção

~~~ {.pascal}
procedure insertion_sort(var v: vetor; tam: longint);
var
   i, j, temp: longint;
begin
    for i := 2 to tam do
    begin
        temp := v[i];
        j := i;
        while ((j > 1) and (v[j-1] > temp)) do
        begin
            v[j] := v[j-1];
            j := j - 1;
        end;
        v[j] := temp;
    end;
end;
~~~
