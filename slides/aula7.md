---
author: Daniel Oliveira
title: Comandos de desvio e repetição IV
subtitle: Aula 7 - Alg1 - Turma C
date: Março/2019
header-includes:
    - \input{babel.tex}
---

# Problema: Calcular o Máximo Divisor Comum (MDC)
\pause

~~~ {.pascal}
program mdcdemorado;
var a, b, cont, mdc : Longint;
begin
    read (a, b);
    cont := 1;
    while (cont < a) and (cont < b) do
    begin
        if (a mod cont = 0) and (b mod cont = 0) then
            mdc := cont;
        cont := cont + 1;
    end;

    writeln('mdc = ', mdc);
End.
~~~

\pause
- O algoritmo pode ser melhorado? Quanto vezes o laço while vai repetir para as entradas 154896 e 32564879?

# Problema: Máximo Divisor Comum (MDC) por Euclides

- Euclides propôs um algoritmo eficiente
    - Um dos primeiros algoritmos propostos (ano 300 a.c)
\pause

~~~ {.pascal}
program mdcporeuclides;
var a, b, resto : Longint;
begin
    read (a, b) ;
    resto:= a mod b;
    while resto <> 0 do
    begin
        a:= b;
        b:= resto ;
        resto:= a mod b;
    end;
    writeln ('mdc = ', b);
End.
~~~


# Problema: Verificar se um número é perfeito

- Um inteiro positivo N é perfeito se for igual a soma de seus divisores positivos
diferentes de N
    - Exemplo: 6 é perfeito pois 1 + 2 + 3 = 6 
\pause

\footnotesize

~~~ {.pascal}
Program perfeito;
Var a, i, soma: Longint;
Begin
    read(a);
    soma := 0;
    i := 1;
    while i < a do
    begin
        if a mod i = 0 then
            soma := soma + i;
        i := i + 1;
    end;
    if soma = a then
        writeln(a,' eh perfeito')
    else
        writeln(a,' nao eh perfeito')
End.
~~~

\normalsize

# Problema: Verificar se um número é de Armstrong
- Um número de n-dígitos é um número de Armstrong se a soma de seus dígitos elevados a potência de n é igual a ele mesmo
    - $6 = 6^1$
    - $371 = 3^3 + 7^3 + 1^3$
\pause

\scriptsize

~~~ {.pascal}
Program armstrong;
Uses math;
Var a, num, digitos, soma: Longint;
Begin
    read(a);
    digitos := Trunc( log10(a) + 1 );(*calcula quantidade de digitos*)
    soma := 0;
    num := a;
    while num > 0 do
    begin
        soma := soma + Trunc(intpower(num mod 10, digitos));
        num := Trunc(num/10);
    end;
    if soma = a then
        writeln(a,' eh um numero de Armstrong')
    else
        writeln(a,' nao eh um numero de Armstrong');
End.
~~~

\normalsize

# Problema: Verificar se um número é de Strong
- Um número de Strong possui a soma do fatorial de cada um de seus digitos igual a ele mesmo
    - 145 = 1! + 4! + 5!
\pause

\tiny

~~~ {.pascal}
Program strong;
Uses math;
Var a, num, soma, fat, cont: Longint;
Begin
    read(a); 
    num := a; 
    soma := 0;
    while num > 0 do
    begin
        cont := num mod 10;
        fat := 1;
        while cont > 0 do
        begin
            fat := fat * cont;
            cont := cont - 1;
        end;
        soma := soma + fat;
        num := Trunc(num/10);
    end;
    if soma = a then
        writeln(a,' eh um numero de Strong')
    else
        writeln(a,' nao eh um numero de Strong');
End.
~~~

\normalsize
