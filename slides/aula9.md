---
author: Daniel Oliveira
title: Vetores I
subtitle: Aula 9 - Alg1 - Turma C
date: Abril/2019
header-includes:
    - \input{babel.tex}
---

# Como armazenar e reusar diversos valores de forma simples?

- Considere o problema: Ler 10 médias de alunos e falar quantos ficaram abaixo da média da turma

\pause
\scriptsize

~~~ {.pascal}
Program media;
Var n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, mediaturma: Real;
cont: Longint;
Begin
    read(n1, n2, n3, n4, n5, n6, n7, n8, n9, n10);
    mediaturma := (n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + n10)/10;
    cont := 0;
    if n1 < mediaturma then cont := cont + 1;
    if n2 < mediaturma then cont := cont + 1;
    if n3 < mediaturma then cont := cont + 1;
    if n4 < mediaturma then cont := cont + 1;
    if n5 < mediaturma then cont := cont + 1;
    if n6 < mediaturma then cont := cont + 1;
    if n7 < mediaturma then cont := cont + 1;
    if n8 < mediaturma then cont := cont + 1;
    if n9 < mediaturma then cont := cont + 1;
    if n10 < mediaturma then cont := cont + 1;
    writeln('media=',mediaturma,'; alunos abaixo da media: ',cont);
End.
~~~

\normalsize

# Como armazenar e reusar diversos valores de forma simples?

- Considere o problema: Ler 30 médias de alunos e falar quantos ficaram abaixo da média da turma

\pause
\scriptsize

~~~ {.pascal}
Program media;
Var n1, n2, n3, n4, n5, n6, n7, n8, n9, ..., n30, mediaturma: Real;
cont: Longint;
Begin
    read(n1, n2, n3, n4, n5, n6, n7, n8, n9, ..., n30);
    mediaturma := (n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + ... + n30)/30;
    cont := 0;
    if n1 < mediaturma then cont := cont + 1;
    if n2 < mediaturma then cont := cont + 1;
    if n3 < mediaturma then cont := cont + 1;
    if n4 < mediaturma then cont := cont + 1;
    if n5 < mediaturma then cont := cont + 1;
    if n6 < mediaturma then cont := cont + 1;
    if n7 < mediaturma then cont := cont + 1;
    if n8 < mediaturma then cont := cont + 1;
    if n9 < mediaturma then cont := cont + 1;
    ...
    if n30 < mediaturma then cont := cont + 1;
    writeln('media=',mediaturma,'; alunos abaixo da media: ',cont);
End.
~~~

\normalsize


# Estruturas de dados

- Modo de  armazenar e organizar os dados para serem usados eficientemente
    - **Vetores/Matrizes**
    - Tipos de Dados Compostos: registros e estruturas
    - Lista, Fila, Pilha
    - Árvores
    - Heaps
    - Tabelas Hash
    - Estruturas para Grafos
    - ...

# Vetores

- Usar o apenas um nome de variável e acessar diversas posições por índices únicos

    - Exemplo: separar 10 posições de memória para variáveis do tipo Longint

~~~ {.pascal}
        var Vetor: array[1..10] of Longint;
~~~


| índice   | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 |
|----------|---|---|---|---|---|---|---|---|---|----|
| conteúdo | ? | ? | ? | ? | ? | ? | ? | ? | ? | ?  |


# Vetores

- Salvando dados no vetor

~~~ {.pascal}
        var Vetor: array[1..10] of Longint;
        ...
        Vetor[1] = 20;
~~~


| índice   | 1      | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 |
|----------|--------|---|---|---|---|---|---|---|---|----|
| conteúdo | **20** | ? | ? | ? | ? | ? | ? | ? | ? | ?  |



# Vetores

- Salvando dados no vetor

~~~ {.pascal}
        var Vetor: array[1..10] of Longint;
        ...
        Vetor[1] = 20;
        Vetor[3] = 78;
~~~


| índice   | 1  | 2 | 3      | 4 | 5 | 6 | 7 | 8 | 9 | 10 |
|----------|----|---|--------|---|---|---|---|---|---|----|
| conteúdo | 20 | ? | **78** | ? | ? | ? | ? | ? | ? | ?  |

# Vetores

- Salvando dados no vetor

~~~ {.pascal}
        var Vetor: array[1..10] of Longint;
        ...
        Vetor[1] = 20;
        Vetor[3] = 78;
        read(Vetor[10]); (*Usuário entra com '15'*)
~~~


| índice   | 1  | 2 | 3  | 4 | 5 | 6 | 7 | 8 | 9 | 10     |
|----------|----|---|----|---|---|---|---|---|---|--------|
| conteúdo | 20 | ? | 78 | ? | ? | ? | ? | ? | ? | **15** |

# Vetores

- Salvando dados no vetor

~~~ {.pascal}
        var Vetor: array[1..10] of Longint;
        ...
        Vetor[1] = 20;
        Vetor[3] = 78;
        read(Vetor[10]); (*Usuário entra com '15'*)
        vetor[5] = vetor[1] + 10 * vetor[10];
~~~


| índice   | 1  | 2 | 3  | 4 | 5       | 6 | 7 | 8 | 9 | 10 |
|----------|----|---|----|---|---------|---|---|---|---|----|
| conteúdo | 20 | ? | 78 | ? | **170** | ? | ? | ? | ? | 15 |

# Vetores

- Lendo dados do vetor

| índice   | 1  | 2  | 3  | 4 | 5  | 6  | 7  | 8  | 9  | 10 |
|----------|----|----|----|---|----|----|----|----|----|----|
| conteúdo | 20 | 10 | 78 | 7 | 17 | 25 | 12 | 85 | 65 | 15 |

~~~ {.pascal}
        var Vetor: array[1..10] of Longint;
        ...
        i := 1;
        while i <= 10 do
        Begin
            write(vetor[i],' ');
            i := i + 1;
        End;
~~~

Saída na tela: 20 10 78 7 17 25 12 85 65 15

# Vetores

- Operações sobre vetores


~~~ {.pascal}
        var Vetor: array[1..10] of Longint;
        ...
        i := 1;
        while i <= 10 do
        Begin
            vetor[i] := i*2;
            i := i + 1;
        End;
~~~

| índice   | 1 | 2 | 3 | 4 | 5  | 6  | 7  | 8  | 9  | 10 |
|----------|---|---|---|---|----|----|----|----|----|----|
| conteúdo | 2 | 4 | 6 | 8 | 10 | 12 | 14 | 16 | 18 | 20 |

# Vetores

- Operações sobre vetores, soma 1 a todos os elementos

Vetor antes

| índice   | 1 | 2 | 3 | 4 | 5  | 6  | 7  | 8  | 9  | 10 |
|----------|---|---|---|---|----|----|----|----|----|----|
| conteúdo | 2 | 4 | 6 | 8 | 10 | 12 | 14 | 16 | 18 | 20 |


~~~ {.pascal}
        i := 1;
        while i <= 10 do
        Begin
            vetor[i] := vetor[i]+1; 
            i := i + 1;
        End;
~~~

Vetor depois

| índice   | 1 | 2 | 3 | 4 | 5  | 6  | 7  | 8  | 9  | 10 |
|----------|---|---|---|---|----|----|----|----|----|----|
| conteúdo | 3 | 5 | 7 | 9 | 11 | 13 | 15 | 17 | 19 | 21 |



# Outros laços de repetição

\columnsbegin
\column{.5\textwidth}
- For loop

~~~ {.pascal}
        For i:=1 to 10 do
        Begin
            vetor[i] := i*2; 
        End;
~~~

\vspace{2.5em}
\column{.5\textwidth}
- Equivalente usando While

~~~ {.pascal}
        i := 1;
        while i <= 10 do
        Begin
            vetor[i] := i*2;
            i := i + 1;
        End;
~~~

\columnsend

| índice   | 1 | 2 | 3 | 4 | 5  | 6  | 7  | 8  | 9  | 10 |
|----------|---|---|---|---|----|----|----|----|----|----|
| conteúdo | 2 | 4 | 6 | 8 | 10 | 12 | 14 | 16 | 18 | 20 |

# Outros laços de repetição

\columnsbegin
\column{.5\textwidth}
- Repeat-Until

~~~ {.pascal}
        i:=1;
        repeat
            vetor[i] := i*2;
            i := i + 1;
        until i > 10; 
~~~

\vspace{1.3em}
\column{.5\textwidth}
- Equivalente usando While

~~~ {.pascal}
        i := 1;
        while i <= 10 do
        Begin
            vetor[i] := i*2;
            i := i + 1;
        End;
~~~

\columnsend

| índice   | 1 | 2 | 3 | 4 | 5  | 6  | 7  | 8  | 9  | 10 |
|----------|---|---|---|---|----|----|----|----|----|----|
| conteúdo | 2 | 4 | 6 | 8 | 10 | 12 | 14 | 16 | 18 | 20 |

# Outros laços de repetição

\columnsbegin
\column{.5\textwidth}
- For decrementando **downto**

~~~ {.pascal}
        For i:= 10 downto 1 do
        Begin
            write(i,' ');
        End;
~~~

\vspace{2.5em}
\column{.5\textwidth}
- Equivalente usando While

~~~ {.pascal}
        i := 10;
        while i >= 1 do
        Begin
            write(i,' ');
            i := i - 1;
        End;
~~~

\columnsend

\vspace{5em}

Saída na Tela: 10 9 8 7 6 5 4 3 2 1

# Outros laços de repetição

\columnsbegin
\column{.5\textwidth}
- Repeat-Until

~~~ {.pascal}
        i:=10;
        repeat
            write(i,' ');
            i := i - 1;
        until i < 1; 
~~~

\vspace{1.3em}
\column{.5\textwidth}
- Equivalente usando While

~~~ {.pascal}
        i := 10;
        while i >= 1 do
        Begin
            write(i,' ');
            i := i - 1;
        End;
~~~

\columnsend

\vspace{5em}

Saída na Tela: 10 9 8 7 6 5 4 3 2 1


# Voltando ao problema

- Ler 10 médias de alunos e falar quantos ficaram abaixo da média da turma
\pause

\scriptsize

~~~ {.pascal}
Program notas10;
const N_alunos=10;
Var 
    vetor: array[1..N_alunos] of Real;
    soma, media: real;
    i, cont: Longint;
Begin
    soma := 0;
    For i:=1 to N_alunos do
    Begin
        read(vetor[i]);
        soma := soma + vetor[i];
    End;
    media := soma/N_alunos;
    cont:= 0;
    For i:=1 to N_alunos do
    Begin
        if (vetor[i] < media) then
            cont:= cont + 1;
    End;
    writeln('Media da turma: ',media:2:2,', ',cont,' aluno(s) ficaram abaixo');
End.

~~~

# Voltando ao problema - Usando while

- Ler 10 médias de alunos e falar quantos ficaram abaixo da média da turma

\tiny

~~~ {.pascal}
Program notas10_while;
const N_alunos=10;
Var 
    vetor: array[1..N_alunos] of Real;
    soma, media: real;
    i, cont: Longint;
Begin
    soma := 0;
    i := 1;
    while i <= N_alunos do
    Begin
        read(vetor[i]);
        soma := soma + vetor[i];
        i := i + 1;
    End;
    media := soma/N_alunos;
    cont:= 0;
    i := 1;
    while i <= N_alunos do
    Begin
        if (vetor[i] < media) then
            cont:= cont + 1;
        i := i + 1;
    End;
    writeln('Media da turma: ',media:2:2,', ',cont,' aluno(s) ficaram abaixo');
End.

~~~

# Voltando ao problema com mais alunos

- Ler 100 médias de alunos e falar quantos ficaram abaixo da média da turma
\pause

\scriptsize

~~~ {.pascal}
Program notas100;
const N_alunos=100;
Var 
    vetor: array[1..N_alunos] of Real;
    soma, media: real;
    i, cont: Longint;
Begin
    soma := 0;
    For i:=1 to N_alunos do
    Begin
        read(vetor[i]);
        soma := soma + vetor[i];
    End;
    media := soma/N_alunos;
    cont:= 0;
    For i:=1 to N_alunos do
    Begin
        if (vetor[i] < media) then
            cont:= cont + 1;
    End;
    writeln('Media da turma: ',media:2:2,', ',cont,' aluno(s) ficaram abaixo');
End.

~~~

