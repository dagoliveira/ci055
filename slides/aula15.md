---
author: Daniel Oliveira
title: Exercícios Matrizes
subtitle: Aula 15 - Alg1 - Turma C
date: Maio/2019
colorlinks: true
header-includes:
    - \definecolor{Blue}{HTML}{0000FF}
    - \input{babel.tex}
---

# Imagens no formato PGM

- O formato PGM (Portable Graymap Format) é um formato de imagens baseado em um arquivo ASCII
    - Faz parte do formato mais geral Netpbm

\vfill

- Definição:
    - A primeira linha contém o identificador do formato "P2"
    - A segunda linha contém a largura e a altura
    - Terceira linha contém o valor de pixel de intensidade máxima (branco)
    - O restante são os valores dos pixels
        - Valor 0 significa perto
        - Valor 255 significa branco


# Exemplo PGM

~~~
P2
17 7
9
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 3 3 3 3 0 0 6 0 0 0 0 9 9 9 9 0
0 3 0 0 3 0 0 6 0 0 0 0 9 0 0 0 0
0 3 3 3 3 0 0 6 0 0 0 0 9 0 9 9 0
0 3 0 0 3 0 0 6 0 0 0 0 9 0 0 9 0
0 3 0 0 3 0 0 6 6 6 6 0 9 9 9 9 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
~~~

\vfill

Resultado: 
![](./img/pgm-exemplo.png){width="10%"}\



# Problema: Clarear Imagem

- Faça uma programa para clarear uma imagem

\vfill

- Como clarear uma imagem?
    - somar uma constante em todos os valores da matriz, e alterar o valor do pixel de maior valor


# Recorte na imagem

- Fazer um recorte da imagem no centro dela
    - Remover linhas no topo e no final e colunas na esquerda e na direita
    - Atualizar largura, altura e intensidade máxima


# Detectar bordas

- Um pixel é considerado de borda quando a diferença entre seu valor e o de seus vizinhos é maior que um determinado limiar

- Uma das maneiras é calcular a diferença com os quatro vizinhos na vertical e horizontal (vizinhança 4)

Exemplo: **limiar 50**
\columnsbegin
\column{.5\textwidth}
![](./img/totem.png)\

\column{.5\textwidth}

![](./img/totem_borda.png)\
\columnsend
