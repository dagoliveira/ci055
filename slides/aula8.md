---
author: Daniel Oliveira
title: Comandos de desvio e repetição V
subtitle: Aula 8 - Alg1 - Turma C
date: Março/2019
header-includes:
    - \input{babel.tex}
---

# Problema: Calcular o seno

- O seno pode ser calculado pela seguinte série:
    - $seno(x) = \frac{x^1}{1!} -  \frac{x^3}{3!} +  \frac{x^5}{5!} -  \frac{x^7}{7!} +  \frac{x^9}{9!} -  \frac{x^{11}}{11!} +  \frac{x^{13}}{13!} -  \frac{x^{15}}{15!} + ...$

\pause


- Podemos dividir em três partes:
    - inversão de sinal
    - denominador incrementando de 2 em 2
    - numerador incrementando a potência de 2 em 2

# Problema: Calcular o seno (solução orimizada)

\scriptsize

~~~ {.pascal}
program senox;
const itmax=20;
var seno, x, xquadrado, numerador, novotermo, fat : real;
     i, sinal : Longint;
begin
    seno :=  0;
    fat :=  1;
    sinal :=  1;
    read(x);
    xquadrado := x*x ;
    numerador :=  x;
    i :=  1;
    while i <= itmax do 
    begin
        novotermo :=  numerador/fat; (*atualiza  o  termo*)
        seno :=  seno +  sinal*novotermo;
        numerador :=  numerador*xquadrado; (*atualiza  o  numerador*)
        fat :=  2*i*(2*i+1)*fat; (*atualiza  o  denominador*)
        sinal := -sinal; (*inverte o sinal a cada iteração*)
        i :=  i  +  1;
    end;
    writeln('seno(',x:3:6,')= ', seno:3:6);
end.
~~~
