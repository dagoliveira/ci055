---
author: Daniel Oliveira
title: Comandos de desvio e repetição III - Arrays
subtitle: Aula 6 - Alg1 - Turma C
date: Março/2019
header-includes:
    - \input{babel.tex}
---

# Problema: Imprimir a tabuada do 1 ao 10

\pause

\small

~~~ {.pascal}
Program tabuada;
Var i, j: Longint;
Begin
    i := 1;
    while i <= 10 do
    begin
        j := 1;
        while j <= 10 do
        begin
            writeln(i:2,' X ',j:2,' = ',i*j); 
            (*':2' para usar duas casas ao imprimir o 
            numero na tela, mesmo que ele tenha apenas um digito*)
            j := j + 1;
        end;
        writeln;
        i := i + 1;
    end;
End.
~~~
\normalsize


# Problema: Imprimir o fatorial de N

\pause

~~~ {.pascal}
Program fatorial;
Var n, fat: Longint;
Begin
    read(n);
    fat := 1;
    while n >= 1 do
    begin
        fat := fat * n;
        n := n - 1;
    end;
    writeln(fat);
End.
~~~

# Problema: Imprimir o fatorial de 1 até N

\pause

~~~ {.pascal}
Program fatorial_ate_n;
Var i, n, fat: Longint;
Begin
    read(n);
    fat := 1;
    i := 1;
    while i <= n do
    begin
        fat := fat * i;
        writeln(i:3,'!: ',fat);
        i := i + 1;
    end;
End.
~~~

# Problema: calcular número de Euler (e)
- Constante de Napier (Logaritmos) descoberta por Jacob Bernoulli (Juros Compostos). Pode ser aproximada pela série:
    - e = $\frac{1}{0!} + \frac{1}{1!} + \frac{1}{2!} + \frac{1}{3!} + \frac{1}{4!} + \frac{1}{5!} + \frac{1}{6!} + \frac{1}{7!} + \frac{1}{8!} + ...$

\pause
\footnotesize

~~~ {.pascal}
Program euler;
Var e: Real;
    i, fat: Longint;
Const numtermos=15; (* Constante, Pascal nao permite que seja 
                    alterada no programa*)
Begin
    e := 0;
    fat := 1;
    i := 1;
    while i <= numtermos do
    begin
        e := e + 1/fat;
        fat := fat * i;
        i := i + 1;
    end;
    writeln('e = ',e);
End.
~~~


# Problema: Contar quantos dígitos um número possui

- Exemplos: 
    - 47 -> 2 dígitos
    - 1923 -> 4 dígitos
\pause

~~~ {.pascal}
Program digitos;
Var a, cont: Longint;
Begin
    read(a);
    cont := 0;
    while a <> 0 do
    begin
        cont := cont + 1;
        a := Trunc(a / 10);
    end;
    writeln(cont);
End.
~~~

# Problema: Calcular a soma dos dígitos de um número

- Exemplos: 
    - 52 -> 5 + 2 -> 7
    - 320 -> 3 + 2 + 0 -> 5
\pause

~~~ {.pascal}
Program somadigitos;
Var a, soma: Longint;
Begin
    read(a);
    soma := 0;
    while a <> 0 do
    begin
        soma := soma + (a mod 10);
        a := Trunc(a / 10);
    end;
    writeln(soma);
End.
~~~



# Problema: calcular a sequência de Fibonacci

- A sequência de Fibonacci é:
    - 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, ...

- Definida pela seguinte recorrência:
    - $F_n = F_{n-1} + F_{n-2}$
    - $F_1 = 1$
    - $F_2 = 1$

# Problema: calcular a sequência de Fibonacci

~~~ {.pascal}
Program fibonacci;
Var ultimo, penultimo, soma, cont: Longint;
Const max=20;
Begin
    ultimo := 1;
    penultimo := 1;
    cont := 3;
    write(penultimo,' ',ultimo,' ');
    while cont <= max do
    begin
        soma := ultimo + penultimo;
        write(soma,' ');
        penultimo := ultimo;
        ultimo := soma;
        cont := cont + 1;
    end;
End.
~~~
