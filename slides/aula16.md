---
author: Daniel Oliveira
title: Exercício Pilha
subtitle: Aula 16 - Alg1 - Turma C
date: Maio/2019
colorlinks: true
header-includes:
    - \definecolor{Blue}{HTML}{0000FF}
    - \input{babel.tex}
---

# Jogo Flood-it

- Jogo on-line em: https://unixpapa.com/floodit/?sz=14&nc=5

\centering

![](./img/floodit.png)\



- Objetivo: 'Inundar' todo o tabuleiro com apenas uma cor

\centering
![](./img/floodit-win.png)\

# Mecânica Flood-it

- Mecânica do jogo:
    - Uma nova cor é escolhida a cada jogada
    - Começando do canto superior esquerdo, todos os vizinhos do elemento [1,1] da matriz que possuem a mesma cor são alterados para a nova cor
        - não são considerados os vizinhos na diagonal (apenas em cima, baixo, direita e esquerda)
    - O jogo termina se todo o tabuleiro ficou com apenas uma cor, ou se o número máximo de jogadas foi alcançado

# Mecânica Flood-it

\columnsbegin
\column{.5\textwidth}
- Tabuleiro inicial:

\centering
![](./img/flood-it-0.png)\

- Amarelo é escolhido, jogada 1:

\centering
![](./img/flood-it-1.png)\

\column{.5\textwidth}
- Marrom é escolhido, jogada 2:

\centering
![](./img/flood-it-2.png)\


- Verde é escolhido, jogada 3:

\centering
![](./img/flood-it-3.png)\
\columnsend

# Implementação Flood-it

- Baixe os arquivos 'floodit.pas' e 'tadpilha.pas' em https://gitlab.com/dagoliveira/ci055/tree/master/codigos/pilha/floodit

\vfill

- Geração da matriz e impressão colorida está pronto em floodit.pas

\vfill

- tadpilha.pas possui uma implementação de pilha que podem usar para resolver a mecânica do jogo

\vfill

- Cores são armazenadas na matriz como números, por exemplo, o número '1' representa a cor azul
    - Logo, os vizinhos que possuem o mesmo número tem a mesma cor

# Descobrir vizinhos de uma mesma cor


- Iniciando com o elemento que esta na primeira linha e primeira coluna, podemos usar o seguinte algoritmo para buscar todos os vizinhos dele que possuem a mesma cor:
    1. Inicialize duas pilhas
        - Uma pilha **P1** para armazenar os vizinhos que precisamos 'visitar'
        - Outra pilha **P2** para armazenar os elementos que já visitamos e precisamos mudar a cor
    2. Insira o elemento [1,1] na pilha P2 e seus vizinhos que possuem a mesma cor na pilha P1
    3. Enquanto a pilha P1 não estiver vazia faça:
        - Remova um elemento na pilha P1
        - Se esse elemento não está na pilha P2, insira-o na pilha P2
        - Insira seus vizinhos que possuem a mesma cor na pilha P1 que não estão na pilha P2

\vfill

- Será necessário uma forma de verificar todos os elementos que estão na pilha P2
