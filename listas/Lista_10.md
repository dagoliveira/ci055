---
title: 'Lista 10'
lang: pt-br
geometry: margin=1.5cm
papersize: a4
---


**1\.** Um vetor real X com n elementos é apresentado como resultado de um sistema de equações lineares Ax = B cujos coeficientes são representados em uma matriz real A(m×n) e os lados direitos das equações (os resultados de cada equação) em um vetor real B de m elementos. Criar um programa em Free Pascal que verifique se o vetor X é realmente solução do sistema dado, imprimindo "sim" caso o vetor informado seja solução do sistema de equações, ou imprimindo "não" caso contrário.

\vspace{1em}
Dica: Comparações entre números reais pode ser um problema dependendo da arquitetura da máquina a ser utilizada. Para solucionar este problema um método a ser utilizado é a comparação por margem de erro.

\vspace{1em}
Exemplo de entrada:

3 3 #tamanho da matriz

1 3 2 #resultado informado

2 1 -3 #Matriz

-1 3 2

3 1 -3

-1 12 0 #lado direito

\vspace{1em}
Saída esperada:

sim


\vspace{2em}

**2\.** Dizemos que uma matriz quadrada inteira é um quadrado mágico se a soma dos elementos de cada linha, a soma dos elementos de cada coluna e a soma dos elementos das diagonais principal e secundária são todos iguais. Exemplo:

8 0 7

4 5 6

3 10 2

\vspace{1em}
é um quadrado mágico pois 8+0+7 = 4+5+6 = 3+10+2 = 8+4+3 = 0+5+10 = 7+6+2 = 8+5+2 = 3+5+7 = 15.

\vspace{1em}
Crie um programa em Free Pascal que leia um valor n representando o tamanho da matriz e leia uma matrix A(n x n) que representa o quadrado e informe se a matriz é um quadrado mágico.

\vspace{1em}
Exemplo de entrada:

3

8 0 7

4 5 6

3 10 2

\vspace{1em}
Saída esperada para o exemplo acima:

sim


\vspace{2em}

**3\.** Seguindo o racíocinio do exercício sobre quadrado mágico. Crie um programa em Free Pascal que leia um inteiro n que representa o tamanho do lado de uma matriz A e uma matriz A(n x n) que representa o quadrado e imprima quantas matrizes não triviais (isto é, não pode ser a matriz que é constituida por apenas um elemento, uma linha e uma coluna) formam um quadrado mágico a partir da matriz fornecida.

\vspace{1em}
Exemplo de entrada:

6

2 7 6 6 7 2

9 5 1 1 5 9

4 3 8 8 3 4

4 9 2 2 9 4

3 5 7 7 5 3

8 1 6 6 1 8

\vspace{1em}
Saída esperada para a entrada acima:

5

\vspace{2em}

**4\.** Um jogo de palavras cruzadas pode ser representado por uma matriz A(n × m) onde cada posição da matriz corresponde a um quadrado do jogo, sendo que 0 indica um quadrado em branco e -1 indica um quadrado preto. Colocar as numerações de início de palavras horizontais e/ou verticais nos quadrados correspondentes (substituindo os zeros), considerando que uma palavra deve ter pelo menos duas letras.

\vspace{1em}
Exemplo de entrada:

5 8

0 -1 0 -1 -1 0 -1 0

0 0 0 0 -1 0 0 0

0 0 -1 -1 0 0 -1 0

-1 0 0 0 0 -1 0 0

0 0 -1 0 0 0 -1 -1


\vspace{1em}
Saída esperada para a entrada anterior:

1 -1 2 -1 -1 3 -1 4

5 6 0 0 -1 7 0 0

8 0 -1 -1 9 0 -1 0

-1 10 0 11 0 -1 12 0

13 0 -1 14 0 0 -1 -1

\vspace{2em}

**5\.** Uma matriz D(8 × 8) pode representar a posiçao atual de um jogo de damas, sendo que 0 indica uma casa vazia, 1 indica uma casa ocupada por uma peça branca e -1 indica uma casa ocupada por uma peça preta. Supondo que as peças pretas estão se movendo no sentido crescente das linhas da matriz D, determinar as posições das peças pretas que:

\vspace{1em}
- podem tomar peças brancas;
- podem mover-se sem tomar peças brancas;
- não podem se mover.

\vspace{1em}
Para este exercício, considere que as peças pretas nunca estarão na última linha do jogo (dama). Caso não ocorra peça para algum dos movimentos possíveis exibir o valor 0 (zero) ao invés da posição.

\vspace{1em}
Exemplo de Entrada 1:

-1 0 -1 0 -1 0 -1 0

0 -1 0 -1 0 -1 0 -1

-1 0 -1 0 -1 0 -1 0

0 0 0 0 0 0 0 0

0 0 0 0 0 0 0 0

0 1 0 1 0 1 0 1

1 0 1 0 1 0 1 0

0 1 0 1 0 1 0 1

\vspace{1em}
Saída esperada para a entrada acima:

tomar: 0

mover: 3-1 3-3 3-5 3-7

ficar: 1-1 1-3 1-5 1-7 2-2 2-4 2-6 2-8

\vspace{1em}
Exemplo de Entrada 2:

-1 0 -1 0 -1 0 -1 0

0 -1 0 -1 0 -1 0 -1

0 0 -1 0 -1 0 -1 0

0 -1 0 0 0 0 0 0

0 0 1 0 0 0 0 0

0 1 0 0 0 1 0 1

1 0 1 0 1 0 1 0

0 1 0 1 0 1 0 1

\vspace{1em}
Saída esperada para a entrada acima:

tomar: 4-2

mover: 2-2 3-3 3-5 3-7

ficar: 1-1 1-3 1-5 1-7 2-4 2-6 2-8


