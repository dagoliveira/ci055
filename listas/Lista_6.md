---
title: 'Lista 6'
lang: pt-br
geometry: margin=1.5cm
papersize: a4
---


**1\.** O tratamento de duplicatas é um fator importante em alguns algoritmos. Um exemplo é um tipo que se pretende a representar um conjunto no sentido da teoria dos conjuntos e matemática. Um conjunto não pode ter elementos repetidos.
Escreva um programa em Free Pascal que leia uma quantidade arbitrária de números inteiros positivos do teclado e determine os números repetidos fornecidos na entrada de dados. O programa deve imprimir apenas uma ocorrência de cada número repetido, mesmo que sejam fornecidas várias duplicatas do mesmo número no momento da entrada. O número zero é o último lido e não deve ser levado em conta na
determinação de repetidos.

Nota: O usuário nunca irá digitar mais do que 100 números para a entrada do programa (excluindo o zero).
Um exemplo de entrada e saı́da é:

Exemplo de entrada:

3 4 5 5 6 7 8 8 9 10 5 5 5 7 7 3 0

Saı́da esperada para a entrada acima:

3 5 7 8

\vspace{2em}

**2\.** Em teoria dos conjuntos uma de suas possı́veis operações é a operação de União. Dados dois conjuntos A e B, a união entre os conjuntos é definida por:

$A \cup B = \{x: x \in A \vee x \in B\}$

Isto é, a União do conjunto A com o conjunto B é um conjunto formado por todo elemento x tal que x é elemento de A ou x é elemento de B. O diagrama de Venn abaixo ilustra bem este conceito.

![](./venn1.png)\

No diagrama apresentado, o conjunto A é composto pelos elementos {1, 2, 3, 7} e o conjunto B é composto pelos elementos {4, 5, 6, 7}, o conjunto A ∪ B é o conjunto {1, 2, 3, 4, 5, 6, 7}.

Escreva um programa em Free Pascal que leia duas sequências com quantidades arbitrárias de valores inteiros positivos, as quais denotam dois conjuntos da matemática. Os valores de uma sequência nunca conterão duplicatas mas podem vir fora de ordem. Cada sequência termina com o valor 0 (que não é elemento de nenhum dos conjuntos). Depois da leitura dos dados, o programa deve determinar e imprimir o conjunto união. O tamanho máximo de cada conjunto é de 200 elementos.
Por exemplo, considere a entrada e a saı́da de dados abaixo:

Exemplo de entrada:

11 7 1 18 6 5 9 0

9 22 4 5 6 18 0


Saı́da esperada para a entrada acima:

11 7 1 18 6 5 9 22 4

\vspace{2em}

**3\.** Triângulo de Pascal é um triângulo aritmético infinito onde são dispostos os coeficientes das expansões binominais. Os números que compõem o triângulo apresentam diversas propriedades e relações. Os números que compõem o triângulo de Pascal são chamados de números binomiais ou coeficientes binomiais. Um número binomial é representado por:

\Large

${n}\choose{p}$

\normalsize

Com n e p números naturais e n ≥ p. O número n é denominado numerador e o p denominador. O número binomial é calculado a partir da relação:

\Large

$\binom{n}{p} = C_{n , p} = \frac{n!}{p!(n-p)!}$

\normalsize


Sendo,
$C_{n , p}$ : combinação simples de n elementos tomados p a p.

O triângulo de Pascal é construı́do colocando-se os números binomiais de mesmo numerador na mesma linha e os coeficientes de mesmo denominador na mesma coluna. Assim, temos:


Linha 0 $\binom{0}{0}$ \
Linha 1 $\binom{1}{0} \binom{1}{1}$\
Linha 2 $\binom{2}{0} \binom{2}{1} \binom{2}{2}$\
Linha 3 $\binom{3}{0} \binom{3}{1} \binom{3}{2} \binom{3}{3}$\
...\
Linha n $\binom{n}{0} \binom{n}{1} \binom{n}{2} \binom{n}{3} ... \binom{n}{n}$\


Propriedades

a ) Todas as linhas têm o número 1 como seu primeiro e último elemento.
De fato, o primeiro elemento de todas as linhas é calculado por:

\Large

$\binom{n}{0} = \frac{n!}{0!(n-0)!} = \frac{n!}{0!n!} = 1$ 

\normalsize


(Considerando 0! = 1)

e o último elemento de todas as linhas é calculado por:

\Large

$\binom{n-1}{p-1} = \frac{n!}{n!} = 1$ 

\normalsize

b ) O restante dos números de uma linha é formado pela adição dos dois números mais próximos da linha acima. Essa propriedade é chamada de Relação de Stifel e é expressa por:

\Large

$\binom{n-1}{p-1} + \binom{n-1}{p}= \binom{n}{p}$ 

\normalsize

Fazer um programa em Free Pascal que leia um número inteiro positivo N que representa o número de linhas de um Triângulo de Pascal, sendo 0 <= N <= 20. Seu programa deve gerar e imprimir os valores de cada linha deste triângulo no monitor de vı́deo do computador. Veja uma exemplo de execução do programa:

Exemplo de entrada:

7


Saída esperada para a entrada acima:

1

1 1

1 2 1

1 3 3 1

1 4 6 4 1

1 5 10 10 5 1

1 6 15 20 15 6 1

\vspace{2em}

**4\.** Em teoria das probabilidades, o paradoxo do aniversário afirma que dado um grupo de 23 (ou mais) pessoas escolhidas aleatoriamente, a chance de que duas pessoas terão a mesma data de aniversário é de mais de 50%. Embora não seja um paradoxo do ponto de vista matemático, ele é chamado assim pois foge da intuição das pessoas e é difı́cil se acreditar que é mais fácil ter duas pessoas que fazem aniversário no mesmo dia em um grupo de 23 pessoas do que ganhar um cara-ou-coroa.
Existe uma prova analı́tica para este resultado, mas aqui a ideia é fazer simulações que corroborem de forma numérica que isto de fato é verdade (simulações analı́ticas não substituem a prova analı́tica formal).

Faça um programa em Free Pascal que leia um inteiro positivo N, sendo 23 ≤ N ≤ 30. N indica o tamanho do vetor de aniversários. Em seguida o programa deve ler 100 vetores de tamanho N. Cada elemento do vetor será composto de um inteiro entre 1 e 365, indicando cada dia do ano. Assim, 1 significa primeiro de Janeiro enquanto que 365 significa 31 de Dezembro. Os anos bissextos não fazem parte deste experimento.
Seu programa deve, para cada vetor, determinar se há coincidência de pelo menos dois aniversários, isto é, se o vetor tem pelo menos um valor repetido. Você deve somar todas as ocorrências positivas (positivos) para cada um dos 100 vetores e imprimir positivos/100.

Nos exemplos abaixo usamos vetores de tamanho 12 e 15 e apenas 5 vetores ao invés de 100, senão os casos de teste ficariam ruim de serem vistos neste arquivo. Mas os casos de teste do FARMA-ALG contém os valores conforme especificado acima no enunciado.

Entrada com 12 datas e 5 vetores:

12

113 115 1 189 115 210 183 133 155 15 317 42

199 192 25 234 168 182 201 348 174 21 346 124

51 27 204 91 11 213 34 64 237 71 177 213

9 208 59 127 203 330 304 159 188 235 236 45

249 231 332 163 253 348 74 286 234 319 191 167


Saı́da esperada para a entrada acima:

0.40

\vspace{1em}

Entrada com 15 datas e 5 vetores:

15

327 288 305 123 22 105 205 12 107 170 218 133 59 28 127

12 12 270 132 153 137 178 225 70 233 318 2 44 304 162

34 295 21 271 80 175 216 138 199 307 80 142 137 161 224

182 310 359 267 184 183 231 112 7 147 9 344 57 122 155

133 94 173 58 130 199 317 167 281 110 328 8 298 167 12


Saı́da esperada para a entrada acima:

0.60

\vspace{1em}

Observação para os que duvidam que os números gerados para os casos de teste sejam realmente aleatórios (precisamente falando pseudo-aleatórios). Aqueles que acreditam podem pular o restante deste texto:


Você pode gerar você mesmo uma entrada e testar no seu computador (não no FARMA-ALG). Você
vai ver que os valores impressos são muito próximos dos disponibilizados nos casos de saı́da, eles não
serão necessariamente iguais justamente porque os números gerados são aleatórios. Mas, como dito, isto
3é apenas no caso de você querer controlar o processo todo para verificar que o chamado paradoxo do
aniversário de fato é verdadeiro.

Segue um exemplo de programa que gera números pseudo-aleatórios.

~~~ {.pascal}
program geraentrada;
var i, j, n: integer;
begin
    randomize;  (* executa uma unica vez no codigo *)
    read (n);
    writeln (n); (* entre com n entre 23 e 30 *)
    for i:= 1 to 100 do
    begin
        for j:= 1 to N-1 do
            write (random(365)+1,' ');
        writeln (random(365)+1);
    end;
end.
~~~
