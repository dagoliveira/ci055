---
title: 'Lista 5'
lang: pt-br
geometry: margin=1.5cm
papersize: a4
---


**1\.** Faça um programa em Free Pascal que leia duas listas de valores inteiros positivos, cada um deles terminado em zero. O zero indica o término da entrada de dados e não deve ser processado. Seu programa deve imprimir as duas listas na ordem inversa da entrada. Considere que o usuário nunca digitará mais do que 100 números antes do zero para cada conjunto.

Exemplo de entrada:

1 2 3 4 5 6 0

9 8 7 6 5 4 3 2 1 0

Saída esperada:

6 5 4 3 2 1

1 2 3 4 5 6 7 8 9

\vspace{2em}

**2\.** Faça um programa em Free Pascal que leia duas listas de valores inteiros positivos, cada um deles terminado em zero. O zero indica o término da entrada de dados e não deve ser processado. Seu programa deve inicialmente testar se as duas listas têm o mesmo número de elementos. Se não tiverem deve imprimir a mensagem "Tamanhos diferentes". Caso tenham o mesmo tamanho, seu programa deve verificar se um conjunto é o inverso do outro. Se forem, imprimir "Sim" e em caso contrário, imprimir "Nao"

Exemplo de entrada 1:

1 2 3 4 5 6 0

9 8 7 6 5 4 3 2 1 0

Saída esperada:

Tamanhos diferentes

\vspace{1em}

Exemplo de entrada 2:

1 2 3 4 5 6 0

6 5 4 3 2 1 0

Saída esperada:

Sim

\vspace{1em}

Exemplo de entrada 3:

1 7 3 3 5 6 0

6 5 4 3 2 1 0

Saída esperada:

Nao


\vspace{2em}

**3\.** Faça um programa em Free Pascal que leia o número N, 1 <= N <= 20 e em seguida leia os nomes dos N times que participaram do campeonato de um certo ano. Em seguida leia a pontuação respectiva que cada um dos times obteve naquele campeonato, supondo (para simplificar) que nenhum time obteve a mesma pontuação. Seu programa deve imprimir o nome do campeão, isto é, o que teve a maior pontuação.

No Free Pascal, nomes (ou frases) podem ser manipulados usando-se o tipo predefinido string. Considere que os nomes dos times não ultrapassam 20 caracteres, por isso pode usar o tipo string20. Neste tipo você pode ler os nomes a partir do teclado como se fossem números, mas para não gerar erros de compilação, substitua todos os seus comandos read por readln, senão seu programa pode gerar erros de execução (runtime error) por causa do modo como o compilador lida com o ENTER.

Exemplo do uso do tipo string:

~~~ {.pascal}
var x, y: string[20];
begin
    readln (x);
    readln (y);
    if x = y then
        writeln ('os nomes sao iguais')
    else
        writeln ('o nome ',x,' eh diferente do nome ',y);
end.
~~~

Ou ainda:

~~~ {.pascal}
var x, y: string[20];
begin
    x:= 'algoritmos 1';
    y:= 'computacao';
    if x = y then
        writeln ('os nomes sao iguais')
    else
        writeln ('o nome ',x,' eh diferente do nome ',y);
end.
~~~

então x < y, pois o Free Pascal usa ordenação lexicográfica para isso.

Exemplo de entrada:

5

XV de Piracicaba

Ferroviaria

Botafogo-RP

Sao Carlense

XV de Jau

75

47

68

82

56

Saida esperada:

O campeao eh o Sao Carlense


\vspace{2em}


**4\.** Considere que um professor tem N alunos em sua turma de Algoritmos e Estruturas de Dados I. Este professor calcula a média final dos alunos após a aplicação de três provas Prova1, Prova2 e Prova3 com pesos para as provas respectivamente de 1, 2 e 3. A média final é o resultado da média ponderada (Prova1 + 2 * Prova2 + 3 * Prova3)/6.

Faça um programa em Free Pascal que leia N, 0 <= N <= 100 do teclado (não precisa testar se o valor lido está fora desta faixa) e em seguida leia três listas de notas para os N alunos. As notas sempre são de 0 a 100, não precisa testar isso. Considere que no lugar dos nomes os alunos são numerados de 1 até N. Seu programa deve imprimir a média final dos N alunos (com duas casas decimais) e, para cada aluno, indicar se foi aprovado (média maior ou igual a 70), se deve fazer a prova final (média maior ou igual a 40 e menor que 70) ou se foi reprovado (média inferior a 40).

Exemplo de entrada: // Observacões

5 // a turma tem 5 alunos

50 30 10 80 85 // notas da prova 1 para os 5 alunos

40 40 60 55 75 // notas da prova 2 para os 5 alunos

80 35 90 20 95 // notas da prova 3 para os 5 alunos


Saída esperada:

Aluno 1: 61.67 Final

Aluno 2: 35.83 Reprovado

Aluno 3: 66.67 Final

Aluno 4: 41.67 Final

Aluno 5: 86.67 Aprovado

