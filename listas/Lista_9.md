---
title: 'Lista 9'
lang: pt-br
geometry: margin=1.5cm
papersize: a4
---


**1\.** Faça um programa em Free Pascal que leia dois inteiros positivos m e n, sendo 1 ≤ m, n ≤ 100, e uma matriz A m×n . O programa deve imprimir “sim” se há elementos repetidos na matriz A, caso contrário deve imprimir “nao”.

Nos casos de teste cada elemento x da matriz A é definido por 1 ≤ x ≤ 1000.

\vspace{1em}
Exemplo de entrada 1:

3 3

1 2 3

4 5 6

7 8 9

Saída esperada para o exemplo acima:
nao

\vspace{1em}
Exemplo de entrada 2:

3 4

1 2 3 4

4 5 6 7

7 8 9 10

\vspace{1em}
Saída esperada para o exemplo acima:

sim


\vspace{2em}
**2\.** Faça um programa em Free Pascal que leia dois inteiros positivos m e n, sendo 1 ≤ m, n ≤ 100, e uma matriz A m×n . O programa deve imprimir o números de linhas e o número de colunas que são nulas, ou seja, quando todos os elementos de uma linha ou coluna são iguais a 0 (zero).
Nos casos de teste cada elemento x da matriz A é definido por 0 ≤ x ≤ 100.

\vspace{1em}
Exemplo de entrada:

4 4

1 0 2 3

4 0 5 6

0 0 0 0

0 0 0 0

\vspace{1em}
Saı́da esperada para o exemplo acima:

linhas: 2

colunas: 1


\vspace{2em}
**3\.** Uma matriz inteira A n×n é considerada uma matriz de permutação se em cada linha e em cada coluna
houver n - 1 elementos nulos e um único elemento igual a 1.

\vspace{1em}
Um exemplo de Matriz de permutação:

~~~
0 1 0 0
0 0 1 0
1 0 0 0
0 0 0 1
~~~

\vspace{1em}
Um exemplo de Matriz que não é de permutação:

~~~
 2 -1  0
-1  2  0
 0  0  1
~~~

\vspace{1em}
Faça um programa em Free Pascal que leia um inteiro positivo n, sendo 1 ≤ m, n ≤ 100, e uma matriz
inteira A n×n . O programa deve imprimir “sim” caso a matriz A seja de permutação, caso contrário deve
imprimir “nao”.

Nos casos de teste cada elemento x da matriz A é definido por 0 ≤ x ≤ 100.

\vspace{1em}
Exemplo de entrada 1:

3

1 0 0

0 1 0

0 0 1

\vspace{1em}
Saı́da esperada para o exemplo acima:

sim

\vspace{1em}
Exemplo de entrada 2:

3

1 0 0

1 0 0

0 1 0

\vspace{1em}
Saı́da esperada para o exemplo acima:

nao



\vspace{2em}
**4\.** Uma matriz quadrada A n×n é considerada triangular quando os elementos que estão acima da sua diagonal principal são todos nulos (matriz triangular inferior) ou quando os elementos que estão abaixo de sua diagonal principal são todos nulos (matriz triangular superior). Vejamos dois exemplos de matrizes triangulares:

~~~
9 8 7 6
0 6 7 3
0 0 2 5
0 0 0 1
~~~

Matriz Triangular Superior.

~~~
6 0 0 0
3 0 0 0
0 4 7 0
7 5 2 1
~~~

Matriz Triangular Inferior.


\vspace{1em}
Faça um programa em Free Pascal que leia um inteiro positivo n, sendo 1 ≤ m, n ≤ 100, e uma matriz inteira A n×n . O programa deve imprimir “sim” caso a matriz A seja triangular, caso contrário deve imprimir “nao”.

Nos casos de teste cada elemento x da matriz A é definido por 0 ≤ x ≤ 100.

\vspace{1em}
Exemplo de entrada 1:

3

1 9 5

0 2 4

0 0 7

\vspace{1em}
Saı́da esperada para o exemplo acima:

sim

\vspace{1em}
Exemplo de entrada 2:

3

1 0 0

5 2 0

4 9 3


\vspace{1em}
Saı́da esperada para o exemplo acima:

sim

\vspace{1em}
Exemplo de entrada 2:

3

1 2 3

4 5 6

7 8 9

\vspace{1em}
Saı́da esperada para o exemplo acima:

nao

